<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Happy Home Builder</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  

  <!-- Favicons -->
  <link href="img/logoo.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <!-- <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->

  <!-- Libraries CSS Files -->
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/f.css" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

  <style>

@font-face {
   font-family: "Nithan";
   src: url(lib/font-awesome/fonts/Nithan.ttf);
   font-weight: 700;
}

* {box-sizing: border-box}
body {font-family: "Nithan","Lato", sans-serif;}

li{     
  padding: 0 5px 0 5px;
}

button[type="submit"] {
    background: linear-gradient(135deg, #ff6811, #ffae18);
    border: 0;
    border-radius: 20px;
    padding: 8px 30px;
    color: #fff;
  }
  
  button[type="submit"]:hover {
    cursor: pointer;
    background: linear-gradient(135deg, #ffae18, #ff6811);
  }

  p{
    font-size : 18px;
  }


.bootstrap-select {
  max-width: 200px;
}
.bootstrap-select .btn {
  background-color: orange;
  border-style: solid;
  border-top: none;
  border-bottom: none;
  border-right: none;
  color: white;
  font-weight: 200;
  padding: 12px 12px;
  font-size: 18px;
  margin-bottom: 10px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  text-align-last: center;
}
.bootstrap-select .dropdown-menu {
  margin: 15px 0 0;
}
select::-ms-expand {
  /* for IE 11 */
  display: none;
}

::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    white;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
   color:    white;
   opacity:  1;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
   color:    white;
   opacity:  1;
}
:-ms-input-placeholder { /* Internet Explorer 10-11 */
   color:    white;
}
::-ms-input-placeholder { /* Microsoft Edge */
   color:    white;
}

::placeholder { /* Most modern browsers support this now. */
   color:    white;
}
input[type=text]{
  background-color: orange;
  width: 200px;
  height: 50px;
  text-align: center;
  font-size:18px;
  padding: 12px 20px;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  color:white;
}

 
</style>


</head>

<body oncontextmenu="return false;">
<header id="header" style="background: linear-gradient(135deg, #ff6811, #ffae18);">
    <div class="container">
      <div id="logo" class="pull-left">
        <a href="index.php#home"><img src="img/logo1.png" alt="" title="" style="max-height: 50px; padding: 0px 0px 10px 0px;"></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
          <li><a href="index.php" style="padding-right: 15px; font-size: 24px;">หน้าแรก</a></li>
          <li><a href="about.php" style="padding-right: 15px; font-size: 24px;">เกี่ยวกับเรา</a></li>
          <li><a href="newpro.php" style="padding-right: 15px; font-size: 24px;">ข่าวและโปรโมชั่น</a></li>
          <li><a href="search.php" style="padding-right: 15px; font-size: 24px;">แบบบ้าน</a></li>
          <li><a href="spec.php" style="padding-right: 15px; font-size: 24px;">วัสดุที่ใช้</a></li>
          <li><a href="video.php" style="padding-right: 15px; font-size: 24px;">วีดีโอ</a></li>
          <li><a href="profile.php" style="padding-right: 15px; font-size: 24px;">บ้านผลงาน</a></li>
          <li><a href="contact.php" style="padding-right: 15px; font-size: 24px;">ติดต่อเรา</a></li>
          <li><a href="index_eng.php" style="padding-right: 15px; font-size: 24px;">EN</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header>
  
<main id="main">
<section id="team" >
<div class="bgcolor">
<div class="container-fluid">
      <div class="container" >
          <br><br> <br><br>
        <div class="row  wow fadeInUp">

          <div class="col-lg-5 col-md-4">
            <div class="contact-about">
            <center><h3>ระบบค้นหา</h3>
            <h3>แบบบ้าน<i class="material-icons" style="font-size: 50px;">search</i></h3><hr>
            <h4>Happy Home Builder</h4>
            </center>
            </div>
          </div>

          <div class="col-lg-7 col-md-6">

<form method="get" action="" name="form">  
<div class="form-group col-lg-6">
<div class="row" >
                  <img src="img/icon/home2.png" style= "height: 40px; margin: 5px 20px 10px 10px; float: left; ">
                  
                  <input id="product_id" name="product_id" placeholder= "รหัสแบบบ้าน" onblur="this.placeholder = 'รหัสแบบบ้าน'"  onfocus="this.placeholder = ''" type="text" >
                  
                  </div>
                  </div>
<div class="form-group col-lg-6">
<div class="row" >
                  <img src="img/icon/style22.png" style= "height: 40px; margin: 5px 20px 10px 10px; float: left; ">
                  
                <select  name="style"  class="form-control selectpicker show-tick" title="รูปแบบ (สไตล์)"  >
                <option value="Modern" style= "font-size: 18px;" >โมเดริ์น (Modern)</option>
                <option value="Contemporary" style= "font-size: 18px;" >คอนเทมโพรารี่ (Contemporary)</option>
                <option value="Classic"style= "font-size: 18px;" >คลาสสิค (Classic)</option>
                <option value="Topical"style= "font-size: 18px;" >ทอปปิคอล (Topical)</option>
                <option value="Resort"style= "font-size: 18px;" >รีสอร์ท (Resort)</option>
                <option value="Rofthome"style= "font-size: 18px;" >ล็อฟโฮม (Rofthome)</option>
                <option value="Home Office"style= "font-size: 18px;" >โฮมออฟฟิศ (Home Office)</option>
                <option value="Plant"style= "font-size: 18px;" >โรงงาน (Plant)</option>
                <option value="Thai Style"style= "font-size: 18px;" >ทรงไทยประยุกต์ (Thai Style)</option>
              </select>
                  
                  </div>
                  </div>
                  

                  <div class="form-group col-lg-6">
                  <div class="row" >
                  <img src="img/icon/size2.png" style= "height: 40px; margin: 5px 20px 10px 10px;float: left;">
                 
                  <select  name="home_type" class="form-control selectpicker show-tick" title="ระบบการก่อสร้าง" >
                  <option value="CastinPlace"style= "font-size: 18px;" >ระบบหล่อในที่</option>
                <option value="PrefabHome"style= "font-size: 18px;">ระบบบ้านสำเร็จรูป</option>
              </select>
                  </div>
                  </div>

                  
<div class="form-group col-lg-6">
<div class="row" >
                  <img src="img/icon/facility2.png" style= "height: 40px; margin: 5px 20px 10px 10px; float: left;">
                  <select  name="size"class="form-control selectpicker show-tick" title="พื้นที่ใช้สอย"  >
                <option value="100200"style= "font-size: 18px;" >100 - 200 ตร.ม.</option>
                <option value="201300"style= "font-size: 18px;" >201 - 300 ตร.ม.</option>
                <option value="301400"style= "font-size: 18px;" >301 - 400 ตร.ม.</option>
                <option value="401500"style= "font-size: 18px;" >401 - 500 ตร.ม.</option>
                <option value="501600"style= "font-size: 18px;" >501 - 600 ตร.ม.</option>
                <option value="601700"style= "font-size: 18px;" >601 - 700 ตร.ม.</option>
                <option value="701800"style= "font-size: 18px;" >701 - 800 ตร.ม.</option>
                <option value="801"style= "font-size: 18px;" >มากกว่า 800 ตร.ม.</option>
              </select>
                  </div>
                  </div>

               <div class="form-group col-lg-6">
               <div class="row" >
                  <img src="img/icon/front22.png" style= "height: 40px; margin: 5px 20px 10px 10px;float: left;">
                  <select name="land" class="form-control selectpicker show-tick" title="ขนาดที่ดินทั้งหมด"  >
                <option value="001100"style= "font-size: 18px;" >1 - 100 ตร.ว.</option>
                <option value="101200"style= "font-size: 18px;" >101 - 200 ตร.ว.</option>
                <option value="201300" style= "font-size: 18px;" >201 - 300 ตร.ว.</option>
                <option value="301400" style= "font-size: 18px;" >301 - 400 ตร.ว.</option>
                <option value="401500" style= "font-size: 18px;" >401 - 500 ตร.ว.</option>
                <option value="501600" style= "font-size: 18px;" >501 - 600 ตร.ว.</option>
                <option value="601"style= "font-size: 18px;" >มากกว่า 600 ตร.ว.</option>
              </select>
                  </div>
                  </div>
                 


                 

               <input type="hidden" name="istype" value="<?php $home_type ="0";
if (isset($_GET['home_type'])&&$_GET['home_type']<>"") {
  $home_type = $_GET['home_type'];
}else if(isset($_GET['istype'])){
  $home_type = $_GET['istype'];
}
echo $home_type;?>">

<div class="form-group col-lg-6">
                  <div class="row" >
                  <img src="img/icon/price2.png" style= "height: 40px; margin: 5px 20px 10px 10px; float: left;">
                  <select  name="cost" class="form-control selectpicker show-tick" title="งบประมาณ"  >
                <option value="1000000"style= "font-size: 18px;" >1,000,000 - 2,000,000 บ.</option>
                <option value="2000001"style= "font-size: 18px;" >2,000,001 - 3,000,000 บ.</option>
                <option value="3000001"style= "font-size: 18px;">3,000,001 - 4,000,000 บ.</option>
                <option value="4000001"style= "font-size: 18px;">4,000,001 - 5,000,000 บ.</option>
                <option value="5000001"style= "font-size: 18px;">5,000,001 - 6,000,000 บ.</option>
                <option value="6000001"style= "font-size: 18px;">6,000,001 - 7,000,000 บ.</option>
                <option value="7000001"style= "font-size: 18px;"> มากกว่า 7,000,000 บ.</option>
              </select>
                  </div>
                  </div>

                  <div class="form-group col-lg-6">
                  <div class="row" >
                  <img src="img/icon/level2.png" style= "height: 40px; margin: 5px 20px 10px 10px; float: left;">
                  <select  name="layer" class="form-control selectpicker show-tick" title="จำนวนชั้น"  >
                <option value="1"style= "font-size: 18px;" >1 ชั้น</option>
                <option value="2"style= "font-size: 18px;">2 ชั้น</option>
                <option value="3"style= "font-size: 18px;">3 ชั้น</option>
                <option value="4"style= "font-size: 18px;">4 ชั้น</option>
              </select>
              </div>
                  </div>
                

                  
                  
                  
  
  

 <div class="text-center"><button name="submit" type="submit" style="font-size: 20px; margin-top: 10px; " title="Send Message">ค้นหา</button></div></div><br><br>
</form>
        </div>
        </div>
        </div>
      <!-- </div> -->
   <!-- #contact -->
      <div class="container">
      <br><br><br>
        <div class="section-header">
          <h3 class="section-title">แบบบ้าน</h3>
          <span class="section-divider"></span>
          <!-- <p class="section-description">sub Detail</p> -->
        </div>
        <div class="row wow fadeInUp">
        <?php

$url="";

if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}

  

  


$no_of_records_per_page = 9;
$offset = ($pageno-1) * $no_of_records_per_page;

$col=array("style","land","size","cost","home_type","layer","product_id");
$colval=array("","","","","","","","F");


$url="";

for($i = 0 ; $i < count($col) ; $i++){
  if (isset($_GET[$col[$i]])) {
    if($_GET[$col[$i]]<> ''){
    $colval[$i] = (string) $_GET[$col[$i]];
    $url .="&".$col[$i]."=".$colval[$i]."";
    $colval[7] ="T";
    }
  }
}

if (isset($_GET['home_type'])&&isset($_GET['istype'])) {
  if($_GET["home_type"] =="" && $_GET["istype"] <> "0"){
    $colval[4] = $_GET["istype"];
  }
}


$strSQL = "";
$frist = 0;
for($i = 0 ; $i+1 < count($colval) ; $i++){
  if($colval[$i]<> ''){
    
    if($frist == 0){
      $frist = 1;
    }else{
      $strSQL .= " AND ";
    }
     if($i >= 1 and $i <=3){
      if(strlen($colval[$i])  == 6){
      $landmin = (int)substr($colval[$i], 0 ,3);
      $landmax = (int)substr($colval[$i], 3 ,6);
    }else if(strlen($colval[$i])>6){
      $landmin = (int)$colval[$i];
      if($landmin == 7000001 ){
        $landmax = $landmin * 10;
      }else{
        $landmax = $landmin + 999999;
      }
    }else{
      $landmin = (int)substr($colval[$i], 0 ,3);
      $landmax = $landmin *10;
    }
        $strSQL .= " (".$col[$i]." BETWEEN $landmin AND $landmax ) ";
    }else{
      $strSQL .= " homedetail.".$col[$i]." = '".$colval[$i]."' ";
  }
  }
}



include 'connect.php'; 

if($colval[7] == "T"){
  $total_pages_sql = "SELECT COUNT(*) FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id  WHERE  {$strSQL} AND home_pic.show_order_pic = 1 " ;
  $sql = "SELECT * FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id ";
  $sql .= " WHERE  {$strSQL} AND home_pic.show_order_pic = 1 ORDER BY homedetail.`product_id` DESC LIMIT $offset, $no_of_records_per_page";
}else{
  $total_pages_sql = "SELECT COUNT(*) FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id  WHERE home_pic.show_order_pic = 1  " ;
  $sql = "SELECT * FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id ";
  $sql .= "WHERE home_pic.show_order_pic = 1 ORDER BY homedetail.`product_id` DESC LIMIT $offset, $no_of_records_per_page";
}


$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);
// echo $sql;

$res_data = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res_data)){
  echo "<div class=\"col-lg-4\">";
  echo "<div class=\"member\">";
  echo "<div class=\"pic\"><img src=\"imghome/".$row["product_id"]."/".$row["name_pic"]."\"></div>";
  echo "<div style=\"background: #fff; padding: 10px 10px 10px 10px; \">";
  echo "<a href=\"home.php?homeno=".$row["product_id"]."&hometype=".$row["home_type"]."\"><h1><img src=\"img/icon/home.png\" alt=\"House\" height=\"40\" width=\"40\"> ".$row["product_id"]."</a></h1><hr>";
  echo "<div class=\"box col-lg-6\"><p>รูปแบบ : ".$row["style"]."</p>";
  
  if($row["home_type"] == "CastinPlace"){
    echo "</div><div class=\"box col-lg-6\"><p>ก่อสร้าง : ระบบหล่อในที่</p>";
  }else{
    echo "</div><div class=\"box col-lg-6\"><p>ก่อสร้าง : ระบบสำเร็จรูป</p>";
  }

  echo "</div><div class=\"box col-lg-7\"><p>พื้นที่ใช้สอย : ".$row["size"]." ตร.ม.</p>";
  echo "</div><div class=\"box col-lg-5\"><p>จำนวนชั้น : ".$row["layer"]."</p>";
  echo "</div><div class=\"box col-lg-12\"><p>หน้ากว้างตัวบ้าน : ".$row["space"]." ม.</p>";
  echo "</div><div><center><p>ที่ดินทั้งหมด : ".$row["land"]." ตร.ว.</p></center>";
  echo "</div><div><center><p style=\" font-size: 30px; color: orange;\">ราคา : ".number_format($row["cost"]).".-</p></center>";
  if($row["totalvisit"] == null){
    $visit = 0;
  }else{
    $visit  = $row["totalvisit"];
  }
  echo "</div>";
  echo "</div>";
  echo "</div>";
  echo "</div>";
}

mysqli_close($conn);
if($pageno <= $total_pages+1 ) {
  if($pageno <= 3 ) {
    $start = 1;
  }else if($total_pages-2 <$pageno){
    $start = $total_pages - 4;
  }else{
    $start = $pageno-2;
  }
}

?>
<div class="col-lg-12"></div>
     <div class="col-lg-4 col-md-6"></div>
     <div class="col-lg-4 col-md-6">
       <br><br><center>
<nav aria-label="Page navigation example wow fadeInUp ">
  <ul class="pagination justify-content-center">
    <li class="page-item  <?php if($pageno <= 1){ echo 'disabled'; } ?>">
      <a class="page-link" href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=1".$url; } ?>" tabindex="-1">หน้าแรก</a>
    </li>
  
    <?php for($i=$start;$i<=$start+4;$i++){   ?> 
  <li class="page-item  <?php if( $i > $total_pages  ){ echo 'disabled'; }elseif($pageno ==$i){echo 'active';} ?>"><a class="page-link" href="<?php if( $i > $total_pages ){ echo '#'; } else { echo "?pageno=".$i.$url;}?>"><?php echo $i; ?></a></li>

 <?php } ?>

    <li class="page-item <?php if($total_pages == 0){ echo 'disabled'; }else if($total_pages == $pageno){ echo 'disabled';}?>">
      <a class="page-link  " href="<?php if($total_pages == 0 ){ echo '#'; }else { echo "?pageno=".$total_pages.$url;}?>">หน้าสุดท้าย</a>
    </li>
  </ul>
</nav>
</center>
</div>
<div class="col-lg-4 col-md-6"></div></div>
</div>
    </section><!-- #team -->
    <?php include 'footer.php';?>
</main>


  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <!-- <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script> -->
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <!-- <script src="contactform/contactform.js"></script> -->

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</body>
</html>