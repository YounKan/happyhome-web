<?php session_start();
if ( $_SESSION['loggedin'] != true) {
    header('Location: login.php');
} 
 include 'nav.php';?>



 <!-- PAGE CONTAINER-->
 <div class="page-container">
            <!-- MAIN CONTENT-->
            <div class="main-content">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">


                    <div class="row">
                            <div class="col-lg-12">
                                <!-- USER DATA-->
                                <div class="user-data m-b-30">
                                    <h3 class="title-3 m-b-30">
                                        <i class="zmdi zmdi-account-calendar"></i>ติดต่อสอบถาม</h3>
                                    <div class="filters m-b-45">

                                        
                                    </div>
                                    <div class="table-responsive table-data">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>ข้อมูลผู้ติดต่อ</td>
                                                    <td>หัวข้อ</td>
                                                    <td>เนื้อหา</td>
                                                    <td>เวลา</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>


  <?php

include '../connect.php';


   if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}

$no_of_records_per_page = 6;
$offset = ($pageno-1) * $no_of_records_per_page;
  $total_pages_sql = "SELECT COUNT(*) FROM contact";
  $sql = "SELECT * FROM contact ORDER BY date LIMIT $offset, $no_of_records_per_page";
  

  $result = mysqli_query($conn,$total_pages_sql);
  $total_rows = mysqli_fetch_array($result)[0];
  $total_pages = ceil($total_rows / $no_of_records_per_page);
  
  $res_data = mysqli_query($conn,$sql);
  while($row = mysqli_fetch_array($res_data)){
    echo "<tr>";
    echo "<td>";
    echo " <div class=\"table-data__info\">";
    echo "   <h6>".$row["name"]."</h6>";
    echo "    <span>";
    echo "        <a href=\"#\">".$row["email"]."</a>";
    echo "    </span><br>";
    echo "     <span>";
    echo "         <a href=\"#\">".$row["tel"]."</a>";
    echo "     </span>";
    echo " </div>";
    echo "</td>";
    echo "<td>";
    echo " <div class=\"table-data__info\">";
    echo "   <h6>".$row["subject"]."</h6>";
    echo " </div>";
    echo "</td>";
    echo "<td>".$row["message"]."";
    echo "</td>";
    echo "<td>";
    echo " <div class=\"table-data__info\">";
    echo "   <h6>".$row["date"]."</h6>";
    echo " </div>";
    echo "</td>";
    echo "<td>";
    echo " <div class=\"table-data__info\">";
    if($row["status"] == 0){
        echo " <button type=\"button\" onclick=\"location.href = 'http://happyhomebuilder.co.th/admin/update_status.php?id=".$row["id"]."';\" class=\"btn btn-danger\">ติดต่อกลับ</button>";
    }else{
        echo " <button type=\"button\" class=\"btn btn-success\">ติดต่อกลับ</button>";
    }

  
    echo " </div>";
    echo "</td>";
    echo "<td>";
    echo "<div class=\"table-data-feature\">";
    echo " <button onclick=\"location.href = 'http://happyhomebuilder.co.th/admin/del_contact.php?id=".$row["id"]."';\"class=\"item\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\">";
    echo "   <i class=\"zmdi zmdi-delete\"></i>";
    echo " </button>";
    echo " </div>";
    echo "</td>";
    echo "</tr>";
  }
  if($pageno <= $total_pages+1 ) {
    if($pageno <= 3 ) {
      $start = 1;
    }else if($total_pages-2 <$pageno){
      $start = $total_pages - 4;
    }else{
      $start = $pageno-2;
    }
  }

  
  
  ?>                                          
                                                
                                                
                                            
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                    
                                </div>
                                <!-- END USER DATA-->
                                
<center>
     <div class="col-lg-12 col-md-6">
       <br><br>
<nav aria-label="Page navigation example wow fadeInUp ">
  <ul class="pagination justify-content-center">
  <li class="page-item <?php if($pageno <= 1){ echo 'disabled'; } ?>">
      <a class="page-link" href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=1"; } ?>" tabindex="-1">Frist</a>
    </li>
  
    <?php for($i=$start;$i<=$start+4;$i++){   ?> 
  <li class="page-item <?php if( $i > $total_pages  ){ echo 'disabled'; }elseif($pageno ==$i){echo 'active';} ?>"><a class="page-link" href="<?php if( $i > $total_pages ){ echo '#'; } else { echo "?pageno=".$i;}?>"><?php echo $i; ?></a></li>

 <?php } ?>

    <li class="page-item <?php if($total_pages == 0){ echo 'disabled'; }else if($total_pages == $pageno){ echo 'disabled';} ?>">
      <a class="page-link " href="<?php if($total_pages == 0 ){ echo '#'; } else { echo "?pageno=".$total_pages;}?>">Last</a>
    </li>
  </ul>
</nav>
</center>  
                            </div>
                            
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>