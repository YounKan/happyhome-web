
<?php ob_start(); include 'nav.php';?>

<script type="text/javascript">
    function changeTxt(value){
        $('#pic_id').val(value);
    }
</script>

<?php

include '../connect.php'; 

if (isset($_POST['savebutton'])) {
    $countfiles = count($_FILES['file']['name']);

    $countresult = 0;

    $col=array("product_id","style","home_type","layer","land","size","space","cost","bedroom","bathroom","kitchen","diningroom","livingroom","storeroom","tabernacle","balcony","housekeeper");
    $colval = array();
    for($i = 0 ; $i < count($col) ; $i++){
        if(empty($_POST[$col[$i]])){
            array_push($colval,"0");
        }else{
            array_push($colval,trim($_POST[$col[$i]]));
        }
        echo $colval[$i];
    }
    $error = "";

    $result= false;

    $check = TRUE;
    $frist = 0;
    for($i = 0 ; $i < 8; $i++){
        if(empty($_POST[$col[$i]])){

             if($frist == 0){
                $error = "<strong>การเพิ่มข้อมูลล้มเหลว</strong>";
                $frist = 1;
            }

            switch ($i) {
                case 0:
                    $error .= "<li>โปรดกรอก รหัสแบบบ้าน</li>";
                    break;
                case 1:
                    $error .= "<li>โปรดกรอก รูปแบบ (สไตล์)</li>";
                    break;
                case 2:
                    $error .= "<li>โปรดกรอก ระบบก่อสร้าง</li>";
                    break;
                case 3:
                    $error .= "<li>โปรดกรอก ที่ดินทั้งหมด</li>";
                    break;
                case 4:
                    $error .= "<li>โปรดกรอก พื้นที่ใช้สอย</li>";
                    break;     
                case 5:
                    $error .= "<li>โปรดกรอก จำนวนชั้น</li>";
                    break;
                case 6:
                    $error .= "<li>โปรดกรอก หน้ากว้างตัวบ้าน</li>";
                    break;                   
                default:
                    $error .= "<li>โปรดกรอก งบประมาณ</li>";
                }
                $check = false;
        }
    }    
    if($_FILES['file']['name'][0]==''){
        if($frist == 0){
            $error = "<strong>การเพิ่มข้อมูลล้มเหลว</strong>";
            $frist = 1;
        }

        $error .= "<li>โปรดเลือกรูป</li>";
        $check = false;
  }

  if( $check){

    $check_id = mysqli_query( $conn,"select * from homedetail where product_id='$colval[0]' and home_type = '$colval[2]'");
     
    if(mysqli_num_rows($check_id)>=1){ 
    
        $error = "<strong>การเพิ่มข้อมูลล้มเหลว</strong>";
        $error .= "<li>มีรหัสแบบบ้าน และ ระบบก่อสร้างนี้แล้ว</li>";
         $check = false;
    }else{

        $datetime = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `homedetail` (`product_id`, `style`,`home_type`, `layer`, `land`, `size`, `space`, `cost`, `bedroom`, `bathroom`, `kitchen`, `diningroom`, `livingroom`, `storeroom`, `tabernacle`, `balcony`, `housekeeper`,`isNew`) ";
        $sql .= "VALUES ('$colval[0]', '$colval[1]', '$colval[2]', '$colval[3]', '$colval[4]', '$colval[5]', '$colval[6]', '$colval[7]',  $colval[8], ";
        $sql .= " $colval[9], $colval[10], $colval[11], $colval[12], $colval[13], $colval[14], $colval[15],  $colval[16],1);  ";
        $result =$conn->query($sql);
        
        $check_count = mysqli_query( $conn,"SELECT COUNT(*) FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id WHERE home_pic.product_id='{$colval[0]}'");
           
            if( $check_count){
                
            $order = mysqli_fetch_array($check_count)[0];
        
            }
        
        
        $datetime = date("Y-m-d H:i:s");
                
        for($i=0;$i<$countfiles;$i++){
            $filentype = $_FILES['file']['type'][$i];
            $filentype = substr($filentype, strrpos( $filentype, '/' )+1 );
            $order++;
            $filename = $colval[0].$order.rand(0,100).".".$filentype;
        
            if (!file_exists('../imghome/'.$colval[0])) {
                mkdir('../imghome/'.$colval[0], 0777, true);
            }
        
            $dirfile = '../imghome/'.$colval[0]."/{$filename }";
            
            move_uploaded_file($_FILES['file']['tmp_name'][$i],$dirfile);  
        
            $sql = "INSERT INTO `home_pic` (`product_id`, `name_pic`, `show_order_pic`, `pic_date`) VALUES ('$colval[0]','$filename', $order , '$datetime');";
            $resultpic =$conn->query($sql);
            if ($resultpic) {
                $countresult++;
            }
        
          }

          if ($result && $countresult == $countfiles ) {
            $error = "<strong>การเพิ่มข้อมูลสำเร็จ</strong>";
            header("refresh:1; url=HDinsert.php");
            
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

    }

    }
}
?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">แบบบ้าน</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">การเพิ่มข้อมูลแบบบ้าน</h3>
                                        </div>
                                        <?php if (isset($_POST['savebutton'])) {
				if($result) {
				echo "<div class=\"alert alert-success\">$error </div>";
				}else{
				echo "<div class=\"alert alert-danger\">$error </div>";
				}
			}			
			?>
                                        <hr>
                                        <form action="" method="post" enctype='multipart/form-data'>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">รหัสแบบบ้าน</label>
                                                    <input id="product_id" name="product_id" placeholder="Ex : P070" type="text" class="form-control" aria-required="true" aria-invalid="false"  onChange="changeTxt(this.value)">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="select" class="control-label mb-1">รูปแบบ (สไตล์)</label>
                                                    <select name="style" id="style" class="form-control">
                                                    <option value="">โปรดเลือก</option>
                                                        <option value="Modern">โมเดริ์น (Modern)</option>
                                                        <option value="Contemporary">คอนเทมโพรารี่ (Contemporary)</option>
                                                        <option value="Classic">คลาสสิค (Classic)</option>
                                                        <option value="Topical">ทอปปิคอล (Topical)</option>
                                                        <option value="Resort">รีสอร์ท (Resort)</option>
                                                        <option value="Rofthome">ล็อฟโฮม (Rofthome)</option>
                                                        <option value="Home Office">โฮมออฟฟิศ (Home Office)</option>
                                                        <option value="Plant">โรงงาน (Plant)</option>
                                                        <option value="Thai Style">ทรงไทยประยุกต์ (Thai Style)</option>
                                                    </select>                                                  
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="select" class="control-label mb-1">ระบบก่อสร้าง</label>
                                                    <select name="home_type" id="home_type" class="form-control">
                                                    <option value="">โปรดเลือก</option>
                                                        <option value="CastinPlace">ระบบหล่อในที่</option>
                                                        <option value="PrefabHome">ระบบบ้านสำเร็จรูป</option>
                                                    </select>                                                  
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">เลือกรูป (สามารถเลือกได้หลายรูป)</label>
                                                    <input type="file" name="file[]" id="file" multiple class="form-control-file">
                                            </div>
                                        </div> 
                                        </div> 
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ที่ดินทั้งหมด (หน่วย ตร.ว.)</label>
                                                    <input id="land" name="land" placeholder="Ex : 95.11" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">หน้ากว้างตัวบ้าน (หน่วย ม.)</label>
                                                    <input id="space" name="space" placeholder="Ex : 30.31 x 12.00" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">พื้นที่ใช้สอย (หน่วย ตร.ม.)</label>
                                                    <input id="size" name="size" placeholder="Ex : 379.70 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="select" class="control-label mb-1">จำนวนชั้น</label>
                                                    <select name="layer" id="layer" class="form-control">
                                                       <option value="">โปรดเลือก</option>
                                                        <option value="1">1 ชั้น</option>
                                                        <option value="2">2 ชั้น</option>
                                                        <option value="3">3 ชั้น</option>
                                                        <option value="4">4 ชั้น</option>
                                                        <option value="5">5 ชั้น</option>
                                                        <option value="6">มากกว่า 5 ชั้น</option>
                                                    </select>                                                  
                                                </div>
                                            </div>
                                        </div>                                     
                                        <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">งบประมาณ</label>
                                                    <input id="cost" name="cost" type="text" placeholder="Ex : 5,900,000 " class="form-control" aria-required="true" aria-invalid="false">
                                                </div>
                                            <div>
                                            <div class="row">
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องนอน</label>
                                                    <input id="bedroom" name="bedroom" placeholder="Ex : 4 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องน้ำ</label>
                                                    <input id="bathroom" name="bathroom" placeholder="Ex : 5 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องครัว</label>
                                                    <input id="kitchen" name="kitchen" placeholder="Ex : 1 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องทานอาหาร</label>
                                                    <input id="diningroom" name="diningroom" placeholder="Ex : 1 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                        </div> 
                                            <div class="row">
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องรับแขก</label>
                                                    <input id="livingroom" name="livingroom" placeholder="Ex : 1 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องเก็บของ</label>
                                                    <input id="storeroom" name="storeroom" placeholder="Ex : 1 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องพระ</label>
                                                    <input id="tabernacle" name="tabernacle" placeholder="Ex : 1 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ระเบียง</label>
                                                    <input id="balcony" name="balcony" placeholder="Ex : 1 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                        </div>  
                                            <div class="row">
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">ห้องแม่บ้าน</label>
                                                    <input id="housekeeper" name="housekeeper" placeholder="Ex : 1 " type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                        </div>                    

                                                <button id="savebutton" name="savebutton" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">ตกลง</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

<?php
  include '../connect.php'; 

  if (isset($_POST['savePicbutton'])) {

      $pic_id = trim($_POST['pic_id']);

      


      $result= false;
      $frist = 0;

      $countfiles = count($_FILES['file']['name']);

      $countresult = 0;

      $check_id = mysqli_query( $conn,"select * from homedetail where product_id='{$pic_id}'");
     
      if(mysqli_num_rows($check_id)>=1){    

        $check_count = mysqli_query( $conn,"SELECT COUNT(*) FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id WHERE home_pic.product_id='{$pic_id}'");
       
        if( $check_count){
            
        $order = mysqli_fetch_array($check_count)[0];

        }

        $val_id = 1;
    }else{
        $val_id = 0;
      
    }

          if(empty($_POST['pic_id']) || $_FILES['file']['name'][0]=='' || $val_id == 0){

                $error = "<strong>การเพิ่มรูปล้มเหลว</strong>";
                if(empty($_POST['pic_id'])){
                    $error .= "<li>โปรดกรอก รหัสแบบบ้าน</li>";
                }
                if($_FILES['file']['name'][0]==''){
                    $error .= "<li>โปรดเลือกรูป</li>";
                }
                if($val_id == 0){
                    $error .= "<li>ไม่พบรหัสบ้านในระบบ</li>";
                }

            
          }else if ($val_id == 1){

            $datetime = date("Y-m-d H:i:s");
            
            for($i=0;$i<$countfiles;$i++){
                $filentype = $_FILES['file']['type'][$i];
                $filentype = substr($filentype, strrpos( $filentype, '/' )+1 );
                $order++;
                $filename = $pic_id.$order.rand(0,100).".".$filentype;

                if (!file_exists('../imghome/'.$pic_id)) {
                    mkdir('../imghome/'.$pic_id, 0777, true);
                }

                $dirfile = '../imghome/'.$pic_id."/{$filename }";
                
                move_uploaded_file($_FILES['file']['tmp_name'][$i],$dirfile);  

                $sql = "INSERT INTO `home_pic` (`product_id`, `name_pic`, `show_order_pic`, `pic_date`) VALUES ('$pic_id','$filename', $order , '$datetime');";
                $result =$conn->query($sql);
                if ($result) {
                    $countresult++;
                }

              }


          }
      
          if ($countresult == $countfiles ) {
            $error = "<strong>การเพิ่มรูปสำเร็จ</strong>";
            header("refresh:1; url=HDinsert.php");
            
        } 




        $conn->close();

  
    }



?>
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">แบบบ้าน</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">การเพิ่มรูปแบบบ้าน</h3>
                                        </div>
                                        <?php if (isset($_POST['savePicbutton'])) {
				if($countresult == $countfiles ) {
				echo "<div class=\"alert alert-success\">$error </div>";
				}else{
				echo "<div class=\"alert alert-danger\">$error </div>";
				}
			}			
			?>
                                        <hr>
                                        <form action="" method="post" novalidate="novalidate" enctype='multipart/form-data'>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">รหัสแบบบ้าน</label>
                                                    <input id="pic_id" name="pic_id" placeholder="Ex : P070" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                            </div>
                                        </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">เลือกรูป (สามารถเลือกได้หลายรูป)</label>
                                                    <input type="file" name="file[]" id="file" multiple class="form-control-file">
                                            </div>
                                        </div> 
                                            </div>
                                                <button id="savePicbutton" name="savePicbutton" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">ตกลง</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

</body>

</html>           