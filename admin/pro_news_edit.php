
<?php ob_start(); include 'nav.php';?>

<script type="text/javascript">
    function changeTxt(value){
        $('#pic_id').val(value);
    }

    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="homenew[]" class="form-control"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

<?php

include '../connect.php'; 

$sql = "SELECT * FROM pro_news  WHERE pro_news_id = '".$_GET["pro_news_id"]."'";

$objQuery = mysqli_query( $conn,$sql);
$arr_val =array();

while($row = mysqli_fetch_assoc($objQuery)){

    array_push($arr_val,$row["pro_news_id"]);
    array_push($arr_val,$row["title"]);
    array_push($arr_val,$row["subtitle"]);
    array_push($arr_val,$row["maintype"]);

}

$sqlhome = "SELECT name , type FROM pro_news LEFT JOIN pro_news_detail ON pro_news.pro_news_id=pro_news_detail.pro_news_id WHERE pro_news_detail.pro_news_id = '".$_GET["pro_news_id"]."'";

$objQueryhome = mysqli_query( $conn,$sqlhome);
$arr_val_home =array();

while($row = mysqli_fetch_assoc($objQueryhome)){
    if($row["type"] == "homedesign"){

    array_push($arr_val_home,$row["name"]);
    }

}

    if (isset($_POST['savebutton'])) {

        $type_pro_news = $_POST['type'];
        $title_pro_news = $_POST['title'];

        if (isset($_POST['homenew'])) {
        $home_pro_news = $_POST['homenew'];
        }else{
        $home_pro_news = array("0");
        }

        if (isset($_POST['subtitle'])) {
            $subtitle_pro_news = $_POST['subtitle'];
        }else{
            $subtitle_pro_news = "";
        }




        $result= false;
  
  
        
       
            if(empty($_POST['type'] || empty($_POST['title']))){
  
                  $error = "<strong>การแก้ไขข่าวสารหรือโปรโมชั่นล้มเหลว</strong>";

                  if(empty($_POST['type'])){
                    $error .= "<li>โปรดเลือกชนิด</li>";
                }if(empty($_POST['title'])){
                    $error .= "<li>โปรดใส่หัวข้อ</li>";
                }
               
  
              
            }else {
                $sql = "UPDATE `pro_news` SET `title`='$title_pro_news',`subtitle`='$subtitle_pro_news',`maintype`='$type_pro_news' WHERE `pro_news_id` =$arr_val[0]";
                $db = mysqli_query( $conn,$sql);

                $id = $arr_val[0];


                $resulthome = 0;

                if($home_pro_news[0] <> "0"){
                    $countresulthome =0;
                    for($i=0;$i< count($home_pro_news);$i++){
                    
        
                        $sqlhome = "INSERT INTO `pro_news_detail` (`pro_news_id`,`type`,`name`) VALUES ('$id','homedesign','{$home_pro_news[$i]}');";
                        $resulthome =$conn->query($sqlhome);
                        if ($resulthome) {
                            $countresulthome++;
                        }
        
                      }
                      if(count($home_pro_news) == $countresulthome){
                        $resulthome = 1 ;
                      }

                }else{
                    $resulthome = 1 ;
                }
  
  
            }
        
            if ($resulthome == 1  ) {
              $error = "<strong>การแก้ไขข้อมูลสำเร็จ</strong>";
              header("refresh:1; url=pro_news_edit.php?pro_news_id={$arr_val[0]}");
              
          } 
    
      }
?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-lg-6">
                            
                                <div class="card">
                                    <div class="card-header">การแก้ไขข่าวสารและโปรโมชั่น</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                           
                                            <h4 class="text-center title-2"> <?php echo $arr_val[1];?></h4>
                                        </div>
                                        <?php if (isset($_POST['savebutton'])) {
				if($resulthome == 1 ) {
				echo "<div class=\"alert alert-success\">$error </div>";
				}else{
				echo "<div class=\"alert alert-danger\">$error </div>";
				}
            }			
			?>
                                        <hr>
                                        <form action="" method="post" novalidate="novalidate" enctype='multipart/form-data'>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                <label for="select" class="control-label mb-1">ชนิด</label>
                                                    <select name="type" id="type" class="form-control">
                                                        <option  value="">Please select</option>
                                                        <option <?php if($arr_val[3] == 'news'){ echo ' selected="selected"'; } ?> value="news">ข่าวสาร</option>
                                                        <option <?php if($arr_val[3] == 'pro'){ echo ' selected="selected"'; } ?> value="pro">โปรโมชั่น</option>
                                                    </select>  
                                                </div>
                                            </div>
                                            </div>

                                             <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">หัวข้อ</label>
                                                    <input id="title" name="title"  value="<?php echo $arr_val[1];?>" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="textarea-input" class=" form-control-label">เนื้อหา</label>
                                                    <textarea name="subtitle" id="subtitle" rows="5" placeholder="Content..." class="form-control"><?php echo $arr_val[2];?></textarea>
                                                </div>
                                            </div>
                                        </div>   
                                           
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                <div class="input_fields_wrap">
                                                <button class="add_field_button btn  btn-info btn-block">เพิ่มแบบบ้าน</button><hr>
   <?php
                                            for($i =0 ; $i < count($arr_val_home) ; $i++){
                                                echo "<div><input value=\"$arr_val_home[$i]\" type=\"text\" name=\"home[]\" class=\"form-control\"/><a href=\"del_pro_news_home.php?id=$arr_val[0]&name=$arr_val_home[$i]\" class=\"remove_field\">Remove</a></div>";
                                            }
   
   
   
   ?>
                                                </div>                                         
                                                </div>
                                            </div>
                                        </div> 
                                        

                                                <button id="savebutton" name="savebutton" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">ตกลง</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <!-- </div> -->


                            <?php
  include '../connect.php'; 

  $check_pic = mysqli_query( $conn,"SELECT COUNT(*) FROM pro_news_detail WHERE pro_news_id = $arr_val[0] AND type= 'img'");
           
    if($check_pic){

        $numpic = mysqli_fetch_array($check_pic)[0];
    }

  if (isset($_POST['savePicbutton'])) {

      $result= false;

      $countfiles = count($_FILES['file']['name']);

      $countresult = 0;

    

          if( $_FILES['file']['name'][0]==''){

                $error = "<strong>การเพิ่มรูปล้มเหลว</strong>";

                if($_FILES['file']['name'][0]==''){
                    $error .= "<li>โปรดเลือกรูป</li>";
                }
            
          }else {

            $check_count = mysqli_query( $conn,"SELECT COUNT(*) FROM pro_news_detail WHERE pro_news_id = $arr_val[0] AND type= 'img'");
         
            if( $check_count){
               
           $order = mysqli_fetch_array($check_count)[0];
     
           }
            
           for($i=0;$i<$countfiles;$i++){
            $filentype = $_FILES['file']['type'][$i];
            $filentype = substr($filentype, strrpos( $filentype, '/' )+1 );
            $order++;
            $filename = $order.rand(0,100).".".$filentype;

            if (!file_exists('../img/pro_news/'.$arr_val[0])) {
                mkdir('../img/pro_news/'.$arr_val[0], 0777, true);
            }

            $dirfile = '../img/pro_news/'.$arr_val[0].'/'.$filename;
            
            move_uploaded_file($_FILES['file']['tmp_name'][$i],$dirfile);  

            $sql = "INSERT INTO `pro_news_detail` (`pro_news_id`,`type`,`name`,`show_order_pic`) VALUES ('$arr_val[0]','img','$filename','$order');";
            $result =$conn->query($sql);
            if ($result) {
                $countresult++;
            }

          }


          }
      
          if ($countresult == $countfiles ) {
            $error = "<strong>การเพิ่มรูปสำเร็จ</strong>";
            header("refresh:1; url=pro_news_edit.php?pro_news_id=$arr_val[0]");
            
        } 

  
    }

    $arr_pic = array();
    $arr_pic_order = array();

  $strSQL = "SELECT * FROM `pro_news` LEFT JOIN pro_news_detail ON pro_news.pro_news_id=pro_news_detail.pro_news_id  WHERE pro_news_detail.pro_news_id  = $arr_val[0] AND pro_news_detail.type = 'img' order by show_order_pic ";
  $countSQL = "SELECT COUNT(*) FROM `pro_news` LEFT JOIN pro_news_detail ON pro_news.pro_news_id=pro_news_detail.pro_news_id   WHERE pro_news_detail.pro_news_id  = $arr_val[0] AND pro_news_detail.type = 'img'";

  

  $result = mysqli_query($conn,$countSQL);
  $count = mysqli_fetch_array($result)[0];


  $objQuery =mysqli_query($conn,$strSQL);
  while($row = mysqli_fetch_assoc($objQuery)){
        array_push($arr_pic,$row["name"]);
        array_push($arr_pic_order,$row["show_order_pic"]);
      }



?>

 <div class="col-lg-6">
 <?php if (isset($_GET["result"])) {
                          $result =   $_GET["result"];
                          $home_id =   $_GET["pro_news_id"];
				if($result == 0) {
				echo "<div class=\"alert alert-success\">ลบรูปของโปโมชั่น : ".$home_id." สำเร็จ  </div>";
				}else{
				echo "<div class=\"alert alert-danger\">ลบรูปของโปโมชั่น : ".$home_id." ไม่สำเร็จ </div>";
				}
            }	
            
            if (isset($_POST['savePicbutton'])) {
				if($countresult == $countfiles ) {
				echo "<div class=\"alert alert-success\">$error </div>";
				}else{
				echo "<div class=\"alert alert-danger\">$error </div>";
				}
            }			
            
			?>
 <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">การเพิ่มรูปข่าวสารและโปรโมชั่น</div>
                                    <div class="card-body">
                                        <form action="" method="post" novalidate="novalidate" enctype='multipart/form-data'>
                                
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">เลือกรูป (สามารถเลือกได้หลายรูป)</label>
                                                    <input type="file" name="file[]" id="file" multiple class="form-control-file">
                                            </div>
                                        </div> 
                                            </div>
                                                <button id="savePicbutton" name="savePicbutton" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">ตกลง</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
<?php

if (isset($_POST['saveorderbutton'])) {
   $orderinpic = $_POST['order_pic'];
   $pic_name = $_POST["pic_name"];

   $sqlup = "UPDATE `pro_news_detail` SET `show_order_pic`={$orderinpic}  WHERE name ='{$pic_name}'";
    $dbup = mysqli_query( $conn,$sqlup);
    echo "<META HTTP-EQUIV=refresh CONTENT=\"0.1;URL=pro_news_edit.php?pro_news_id=$arr_val[0]\">";
}

?>


                                <?php
                                if(count($arr_pic) >= 1){

                                    echo "<div class=\"col-lg-12\">";
                                    echo "<div class=\"au-card recent-report\">";
                                    echo "<div class=\"au-card-inner\">";
                                    echo "<div class=\"chart-info\">";
                                    echo "<div class=\"row\">";
                                    echo "<div class=\"col-lg-10\">";
                                    echo "<form action=\"\" method=\"post\">";
                                    echo "<div class=\"row\">";
                                    echo "<div class=\"col-6\">";
                                    echo "<div class=\"form-group\">";
                                    echo " <label for=\"cc-payment\" class=\"control-label mb-1\">ลำดับการแสดง : </label>";
                                    if($numpic == 1){
                                        echo "<input id=\"order_pic\" readonly name=\"order_pic\"   value=\"".$arr_pic_order[0]."\"  type=\"text\" class=\"form-control\" aria-required=\"true\" aria-invalid=\"false\" >";
                                    }else{
                                        echo "<input id=\"order_pic\" name=\"order_pic\"   value=\"".$arr_pic_order[0]."\"  type=\"text\" class=\"form-control\" aria-required=\"true\" aria-invalid=\"false\" >";
                                    }
                                    
                                    echo "</div></div>";
                                    echo " <input type=\"hidden\" name=\"pic_name\" id=\"pic_name\" value=\"".$arr_pic[0]."\">";
                                    echo "<div class=\"col-5\">";
                                    echo "<div class=\"form-group\" style=\"margin-top: 20px;\">";
                                    echo "<button id=\"saveorderbutton\" name=\"saveorderbutton\" type=\"submit\" class=\"btn btn-lg btn-info btn-block\">";
                                    echo "<span id=\"payment-button-amount\">ตกลง</span>";
                                    echo "</button>";
                                    echo "</div></div></div> </form></div>";
                                    echo "<div class=\"col-lg-2\">";
                                    if($numpic == 1){
                                        echo "<button style=\"margin-top: 20px;\" onclick=\"location.href = 'http://happyhomebuilder.co.th/admin/pro_news_edit.php?pro_news_id=$arr_val[0]';\"  class=\"item\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"delete\">";
                                    }else{
                                        echo "<button style=\"margin-top: 20px;\" onclick=\"location.href = 'http://happyhomebuilder.co.th/admin/del_img_pronews.php?name_pic=".$arr_pic[0]."';\"  class=\"item\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"delete\">";
                                    }
                                   
                                    echo "<i class=\"zmdi zmdi-delete\" style=\"font-size: 40px; right: 16px;\"></i>";
                                    echo "</button></div></div></div>";
                                    echo "<div class=\"recent-report__chart\">";
                                    echo "<img src=\"../img/pro_news/".$arr_val[0]."/".$arr_pic[0]."\" alt=\"\">";
                                    echo "</div>";
                                    echo "</div>";
                                    echo "</div>";

                                }?>
                    
</div>
</div>

<?php

for($i = 1 ; $i < count($arr_pic) ; $i++){ 
                                   echo "<div class=\"col-lg-6\">";
                                    echo "<div class=\"au-card recent-report\">";
                                    echo "<div class=\"au-card-inner\">";
                                    echo "<div class=\"chart-info\">";
                                    echo "<div class=\"row\">";
                                    echo "<div class=\"col-lg-10\">";
                                    echo "<form action=\"\" method=\"post\">";
                                    echo "<div class=\"row\">";
                                    echo "<div class=\"col-6\">";
                                    echo "<div class=\"form-group\">";
                                    echo " <label for=\"cc-payment\" class=\"control-label mb-1\">ลำดับการแสดง : </label>";
                                    echo "<input id=\"order_pic\" name=\"order_pic\"   value=\"".$arr_pic_order[$i]."\"  type=\"text\" class=\"form-control\" aria-required=\"true\" aria-invalid=\"false\" >";
                                    echo "</div></div>";
                                    echo " <input type=\"hidden\" name=\"pic_name\" id=\"pic_name\" value=\"".$arr_pic[$i]."\">";
                                    echo "<div class=\"col-5\">";
                                    echo "<div class=\"form-group\" style=\"margin-top: 20px;\">";
                                    echo "<button id=\"saveorderbutton\" name=\"saveorderbutton\" type=\"submit\" class=\"btn btn-lg btn-info btn-block\">";
                                    echo "<span id=\"payment-button-amount\">ตกลง</span>";
                                    echo "</button>";
                                    echo "</div></div></div> </form></div>";
                                    echo "<div class=\"col-lg-2\">";
                             
                                    echo "<button style=\"margin-top: 20px;\" onclick=\"location.href = 'http://happyhomebuilder.co.th/admin/del_img_pronews.php?name_pic=".$arr_pic[$i]."'\"  class=\"item\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"delete\">";
                                    echo "<i class=\"zmdi zmdi-delete\" style=\"font-size: 40px; right: 16px;\"></i>";
                                    echo "</button></div></div></div>";
                                    echo "<div class=\"recent-report__chart\">";
                                    echo "<img src=\"../img/pro_news/".$arr_val[0]."/".$arr_pic[$i]."\" alt=\"\">";
                                    echo "</div>";
                                    echo "</div>";
                                    echo "</div>";
                                    echo "</div>";


   }


?>
                                </div>
                                </div>
                                </div>
                                    
                                        
                                            
                                    </div>                  
                           
                            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

</body>

</html>           