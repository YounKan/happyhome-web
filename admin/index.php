<?php 

session_start();
if ( $_SESSION['loggedin'] != true) {
    header('Location: login.php');
} 

include 'nav.php';?>
<?php
include '../connect.php'; 

$datetime = date("Y-m-d H:i:s");
$datetimenew = date("Y-m-d 00:00:00");

$check_day = mysqli_query( $conn,"SELECT * FROM day_view WHERE day = '$datetimenew' ");
if(mysqli_num_rows($check_day)>=1)
{
	
  while($row = mysqli_fetch_assoc($check_day)){

	  $todaycount =  $row["dayvisit"]; 
  }
 
}



$dayofweek = date('w', strtotime($datetime)) +1;

$check_week = mysqli_query( $conn,"SELECT * FROM day_view ORDER BY id DESC LIMIT $dayofweek");
if(mysqli_num_rows($check_week)>=1)
{
	$total_week = 0;
  while($row = mysqli_fetch_assoc($check_week)){
    $total_week += $row["dayvisit"];
  }

 
}



$month = date("m");

$check_month = mysqli_query( $conn,"SELECT * FROM day_view ");
if(mysqli_num_rows($check_month)>=1)
{
	$total_month = 0;
  while($row = mysqli_fetch_assoc($check_month)){
	$newDate = date("m", strtotime($row["day"]));
	  if($month == $newDate){
		$total_month += $row["dayvisit"];
	  }
    
  }

 
}



$check_total = mysqli_query( $conn,"SELECT * FROM totalview where page='happyhome' ");
if(mysqli_num_rows($check_total)>=1)
{

  while($row = mysqli_fetch_assoc($check_total)){
	  if($month == $newDate){
		$total = $row["totalvisit"];
	  }
    
  }

 
}


$static = "สถิตคนเข้าชมเว็บไซต์ ทั้งหมด : ".$total." คน,  เดือน : ".$total_month." คน,  สัปดาห์ : ".$total_week." คน,  วันนี้ : ".$todaycount." คน";

?>


        <!-- PAGE CONTAINER-->
        <div class="page-container">

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">สถิตคนเข้าชมเว็บไซต์</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-25">
                            <div class="col-sm-6 col-lg-6">
                                <div class="overview-item overview-item--c1">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                                <h2><?php echo $total; ?></h2>
                                                <span>ทั้งหมด</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="overview-item overview-item--c2">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                            <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                            <h2><?php echo $total_month; ?></h2>
                                                <span>เดือน</span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                    <div class="overview-item overview-item--c3">
                                        <div class="overview__inner">
                                            <div class="overview-box clearfix">
                                                <div class="icon">
                                                <i class="zmdi zmdi-account-o"></i>
                                                </div>
                                                <div class="text">
                                                <h2><?php echo $total_week; ?></h2>
                                                <span>สัปดาห์</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="overview-item overview-item--c4">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                            <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                            <h2><?php echo $todaycount; ?></h2>
                                                <span>วันนี้</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                    <h3 class="title-5 m-b-35">สถิตคนเข้าชมเว็บไซต์ในแต่ละวัน</h3>
                                    </div>
                                    
                                    <div class="table-data__tool-right">
                                    <form method="get" action="">
  ค้นหาวันที่ :
  <input type="date" name="day" value="<?php echo date('Y-m-d'); ?>">
  <button name="submit" type="submit" class="btn btn-primary btn-sm">ค้นหา</button>

</form>
                                    
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>วันที่</th>
                                                <th>จำนวนผู้เข้าชม</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
include '../connect.php'; 
if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
  } else {
    $pageno = 1;
  }
  $no_of_records_per_page = 5;
  $offset = ($pageno-1) * $no_of_records_per_page;


  if(isset($_GET["day"])){
    $total_pages_sql = "SELECT COUNT(*) FROM day_view WHERE day ='".$_GET["day"]." 00:00:00' " ;
    $sql = "SELECT * FROM day_view WHERE day ='".$_GET["day"]." 00:00:00' ORDER BY `id` DESC LIMIT $offset, $no_of_records_per_page";

  }else{
    $total_pages_sql = "SELECT COUNT(*) FROM day_view " ;
    $sql = "SELECT * FROM day_view ORDER BY `id` DESC LIMIT $offset, $no_of_records_per_page";
  }




$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

$res_data = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res_data)){
          echo " <tr class=\"tr-shadow\">";
          echo "<td>".$row["day"]."</td>";
          echo "<td>".$row["dayvisit"]."</td>";
          echo "</tr><tr class=\"spacer\"></tr>";
    }
    mysqli_close($conn);
if($pageno <= $total_pages+1 ) {
  if($pageno <= 3 ) {
    $start = 1;
  }else if($total_pages-2 <$pageno){
    $start = $total_pages - 4;
  }else{
    $start = $pageno-2;
  }
}

?>
                                        </tbody>
                                    </table>
                                </div>


<center>
     <div class="col-lg-12 col-md-6">
       <br><br>
<nav aria-label="Page navigation example wow fadeInUp ">
  <ul class="pagination justify-content-center">
  <li class="page-item <?php if($pageno <= 1){ echo 'disabled'; } ?>">
      <a class="page-link" href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=1"; } ?>" tabindex="-1">Frist</a>
    </li>
  
    <?php for($i=$start;$i<=$start+4;$i++){   ?> 
  <li class="page-item <?php if( $i > $total_pages  ){ echo 'disabled'; }elseif($pageno ==$i){echo 'active';} ?>"><a class="page-link" href="<?php if( $i > $total_pages ){ echo '#'; } else { echo "?pageno=".$i;}?>"><?php echo $i; ?></a></li>

 <?php } ?>

    <li class="page-item <?php if($total_pages == 0){ echo 'disabled'; }else if($total_pages == $pageno){ echo 'disabled';} ?>">
      <a class="page-link " href="<?php if($total_pages == 0 ){ echo '#'; } else { echo "?pageno=".$total_pages;}?>">Last</a>
    </li>
  </ul>
</nav>
</center>  
</div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

</body>

</html>
<!-- end document-->
