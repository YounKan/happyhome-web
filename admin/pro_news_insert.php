
<?php ob_start(); include 'nav.php';?>

<script type="text/javascript">
    function changeTxt(value){
        $('#pic_id').val(value);
    }

    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="home[]" class="form-control"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

<?php

    include '../connect.php'; 

    if (isset($_POST['savebutton'])) {

        $type_pro_news = $_POST['type'];
        $title_pro_news = $_POST['title'];

        if (isset($_POST['home'])) {
        $home_pro_news = $_POST['home'];
        }else{
        $home_pro_news = array("0");
        }

        if (isset($_POST['subtitle'])) {
            $subtitle_pro_news = $_POST['subtitle'];
        }else{
            $subtitle_pro_news = "";
        }




        $result= false;
  
        $countfiles = count($_FILES['file']['name']);
  
        $countresult = 0;
  
        
       
            if($_FILES['file']['name'][0]=='' || empty($_POST['type'] || empty($_POST['title']))){
  
                  $error = "<strong>การเพิ่มข่าวสารหรือโปรโมชั่นล้มเหลว</strong>";

                  if(empty($_POST['type'])){
                    $error .= "<li>โปรดเลือกชนิด</li>";
                }if(empty($_POST['title'])){
                    $error .= "<li>โปรดใส่หัวข้อ</li>";
                }
                  if($_FILES['file']['name'][0]==''){
                      $error .= "<li>โปรดเลือกรูป</li>";
                  }
  
              
            }else {
                $sql = "INSERT INTO `pro_news` (`maintype`,`title`,`subtitle`) VALUES ('$type_pro_news' ,'$title_pro_news','$subtitle_pro_news');";
                $db = mysqli_query( $conn,$sql);

                $get_id = mysqli_query( $conn,"SELECT * FROM pro_news WHERE pro_news_id = LAST_INSERT_ID();");
                $id = mysqli_fetch_array($get_id)[0];

                $check_count = mysqli_query( $conn,"SELECT COUNT(*) FROM pro_news_detail WHERE pro_news_id = '$id' ");
         
         if( $check_count){
            
        $order = mysqli_fetch_array($check_count)[0];
  
        }

              for($i=0;$i<$countfiles;$i++){
                  $filentype = $_FILES['file']['type'][$i];
                  $filentype = substr($filentype, strrpos( $filentype, '/' )+1 );
                  $order++;
                  $filename = $order.rand(0,100).".".$filentype;
  
                  if (!file_exists('../img/pro_news/'.$id)) {
                      mkdir('../img/pro_news/'.$id, 0777, true);
                  }
  
                  $dirfile = '../img/pro_news/'.$id.'/'.$filename;
                  
                  move_uploaded_file($_FILES['file']['tmp_name'][$i],$dirfile);  
  
                  $sql = "INSERT INTO `pro_news_detail` (`pro_news_id`,`type`,`name`,`show_order_pic`) VALUES ('$id','img','$filename','$order');";
                  $result =$conn->query($sql);
                  if ($result) {
                      $countresult++;
                  }
  
                }

                $resulthome = 0;

                if($home_pro_news[0] <> "0"){
                    $countresulthome =0;
                    for($i=0;$i< count($home_pro_news);$i++){
                    
        
                        $sqlhome = "INSERT INTO `pro_news_detail` (`pro_news_id`,`type`,`name`) VALUES ('$id','homedesign','{$home_pro_news[$i]}');";
                        $resulthome =$conn->query($sqlhome);
                        if ($resulthome) {
                            $countresulthome++;
                        }
        
                      }
                      if(count($home_pro_news) == $countresulthome){
                        $resulthome = 1 ;
                      }

                }else{
                    $resulthome = 1 ;
                }
  
  
            }
        
            if ($countresult == $countfiles && $resulthome == 1  ) {
              $error = "<strong>การเพิ่มข้อมูลสำเร็จ</strong>";
              // header("refresh:1; url=A_profile.php");
              
          } 
    
      }
?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-lg-6">
                            
                                <div class="card">
                                    <div class="card-header">แบบบ้าน</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">การเพิ่มข่าวสารและโปรโมชั่น</h3>
                                        </div>
                                        <?php if (isset($_POST['savebutton'])) {
				if($countresult == $countfiles  && $resulthome == 1 ) {
				echo "<div class=\"alert alert-success\">$error </div>";
				}else{
				echo "<div class=\"alert alert-danger\">$error </div>";
				}
			}			
			?>
                                        <hr>
                                        <form action="" method="post" novalidate="novalidate" enctype='multipart/form-data'>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                <label for="select" class="control-label mb-1">ชนิด</label>
                                                    <select name="type" id="type" class="form-control">
                                                        <option value="">Please select</option>
                                                        <option value="news">ข่าวสาร</option>
                                                        <option value="pro">โปรโมชั่น</option>
                                                    </select>  
                                                </div>
                                            </div>
                                            </div>

                                             <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">หัวข้อ</label>
                                                    <input id="title" name="title"  type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="textarea-input" class=" form-control-label">เนื้อหา</label>
                                                    <textarea name="subtitle" id="subtitle" rows="5" placeholder="Content..." class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="cc-payment" class="control-label mb-1">เลือกรูป (สามารถเลือกได้หลายรูป)</label>
                                                    <input type="file" name="file[]" id="file" multiple class="form-control-file">
                                            </div>
                                        </div> 
                                        </div> 
                                        
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                <div class="input_fields_wrap">
    <button class="add_field_button btn  btn-info btn-block">เพิ่มแบบบ้าน</button><hr>
   
</div>                                         
                                                </div>
                                            </div>
                                        </div> 
                                        

                                                <button id="savebutton" name="savebutton" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">ตกลง</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

</body>

</html>           