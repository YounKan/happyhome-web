<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Happy Home Builder</title>
     <!-- Favicons -->
  <link href="../img/logoo.png" rel="icon">

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/a.css" rel="stylesheet" media="all">

      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="../css/p.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="dist/js/bootstrap-select.js"></script>
<style>
@font-face {
   font-family: "Nithan";
   src: url(lib/font-awesome/fonts/Nithan.ttf);
   font-weight: 700;
}

* {box-sizing: border-box}
body {font-family: "Nithan","Lato", sans-serif;}

button[type="submit"] {
    background: linear-gradient(135deg, #ff6811, #ffae18);
    border: 0;
    border-radius: 20px;
    padding: 8px 30px;
    color: #fff;
  }
  
  button[type="submit"]:hover {
    cursor: pointer;
  }

  p{
    font-size : 18px;
  }
</style>

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <img src="images/icon/logoo.png" style="height: 70px;" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                    <li>
                            <a href="index.php">
                            <i class="fa fa-users"></i>สถิตคนเข้าชมเว็บไซต์</a>
                        </li>                   
                        <li>
                            <a href="HD.php">
                            <i class="fa fa-home"></i>แบบบ้าน</a>
                        </li>     
                        <li>
                            <a href="A_profile.php">
                                <i class="fas fa-folder-open"></i>บ้านผลงาน</a>
                        </li>
                        <li>
                            <a href="pro_news.php">
                                <i class="fas fa-table"></i>ข่าวสารและโปรโมชั่น</a>
                        </li>
                        <li>
                            <a href="contact.php">
                                <i class="far fa-check-square"></i>ติดต่อสอบถาม</a>
                        </li>
                        <li>
                            <a href="logout.php">
                                <i class="fa  fa-times"></i>ออกจากระบบ</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/logoo.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
            
                        <li>
                            <a href="index.php">
                            <i class="fa fa-users"></i>สถิตคนเข้าชมเว็บไซต์</a>
                        </li>                   
                        <li>
                            <a href="HD.php">
                            <i class="fa fa-home"></i>แบบบ้าน</a>
                        </li>     
                        <li>
                            <a href="A_profile.php">
                                <i class="fas fa-folder-open"></i>บ้านผลงาน</a>
                        </li>
                        <li>
                            <a href="pro_news.php">
                                <i class="fas fa-table"></i>ข่าวสารและโปรโมชั่น</a>
                        </li>
                        <li>
                            <a href="contact.php">
                                <i class="far  fa-check-square"></i>ติดต่อสอบถาม</a>
                        </li>
                        <li>
                            <a href="logout.php">
                                <i class="fa  fa-times"></i>ออกจากระบบ</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->


    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>
