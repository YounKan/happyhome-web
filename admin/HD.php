<?php session_start();
if ( $_SESSION['loggedin'] != true) {
    header('Location: login.php');
} 
 include 'nav.php';?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- MAIN CONTENT-->
            <div class="main-content">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                        
                        <center>               
                        <div class="col-lg-9">
                        <?php if (isset($_GET["result"])) {
                          $result =   $_GET["result"];
                          $home_id =   $_GET["id"];
                          $home_type =$_GET["type"] ;
				if($result == 0) {
				echo "<div class=\"alert alert-success\">ลบแบบบ้าน : ".$home_id." ระบบก่อสร้าง : ".$home_type." สำเร็จ</div>";
				}else{
				echo "<div class=\"alert alert-danger\">ลบแบบบ้าน : ".$home_id." ระบบก่อสร้าง : ".$home_type."  ไม่สำเร็จ </div>";
				}
			}			
			?>
                                <div class="card">
                                <div class="card-header">ค้นหาแบบบ้าน</div>
                                    <div class="card-body">
                                        <form action="" method="get">
                                        <div class="row">
                                        <div class="col-3">
                                            <div class="form-group">
                                            <label for="cc-payment" class="control-label mb-1">รหัสแบบบ้าน</label>
                                                <input id="product_id" name="product_id" placeholder="Ex : P070" type="text" class="form-control" aria-required="true" aria-invalid="false" >                                       
                                                </div>
                                            </div>
                                            <div class="col-3">
                                            <div class="form-group">
                                                    <label for="select"  class="control-label mb-1">รูปแบบ (สไตล์)</label>
                                                    <select name="style" id="style" class="form-control">
                                                        <option value="">โปรดเลือก</option>
                                                        <option value="Modern">โมเดริ์น (Modern)</option>
                                                        <option value="Contemporary">คอนเทมโพรารี่ (Contemporary)</option>
                                                        <option value="Classic">คลาสสิค (Classic)</option>
                                                        <option value="Topical">ทอปปิคอล (Topical)</option>
                                                        <option value="Resort">รีสอร์ท (Resort)</option>
                                                        <option value="Rofthome">ล็อฟโฮม (Rofthome)</option>
                                                        <option value="Home Office">โฮมออฟฟิศ (Home Office)</option>
                                                        <option value="Plant">โรงงาน (Plant)</option>
                                                        <option value="Thai Style">ทรงไทยประยุกต์ (Thai Style)</option>
                                                    </select>                                                  
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="select" class="control-label mb-1">ขนาดที่ดินทั้งหมด</label>
 
                                                    <select name="land" id="land" class="form-control" >
                                                    <option value="">โปรดเลือก</option>
                                                        <option value="001100">1 - 100 ตร.ว.</option>
                                                        <option value="101200">101 - 200 ตร.ว.</option>
                                                        <option value="201300" >201 - 300 ตร.ว.</option>
                                                        <option value="301400" >301 - 400 ตร.ว.</option>
                                                        <option value="401500">401 - 500 ตร.ว.</option>
                                                        <option value="501600" >501 - 600 ตร.ว.</option>
                                                        <option value="601">มากกว่า 600 ตร.ว.</option>
                                                    </select>                                                
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="select" class="control-label mb-1">พื้นที่ใช้สอย</label>
                                                    <select name="size" id="size" class="form-control">
                                                    <option value="">โปรดเลือก</option>
                                                    <option value="100200">100 - 200 ตร.ม.</option>
                                                    <option value="201300">201 - 300 ตร.ม.</option>
                                                    <option value="301400">301 - 400 ตร.ม.</option>
                                                    <option value="401500">401 - 500 ตร.ม.</option>
                                                    <option value="501600">501 - 600 ตร.ม.</option>
                                                    <option value="601700">601 - 700 ตร.ม.</option>
                                                    <option value="701800">701 - 800 ตร.ม.</option>
                                                    <option value="801900">801 - 900 ตร.ม.</option>
                                                    <option value="9011000">901 - 1000 ตร.ม.</option>
                                                    <option value="10001">มากกว่า 1000 ตร.ม.</option>
                                                    </select>                                                  
                                                </div>
                                            </div>
                                            </div>
                                            <div class="row">
                                            <div class="col-3">
                                            <div class="form-group">
                                                    <label for="select" class="control-label mb-1">ระบบการก่อสร้าง</label>
                                                    <select name="home_type" id="home_type" class="form-control">
                                                    <option value="">โปรดเลือก</option>
                                                        <option value="CastinPlace">ระบบหล่อในที่</option>
                                                        <option value="PrefabHome">ระบบบ้านสำเร็จรูป</option>
                                                    </select>                                                  
                                                </div>
                                            </div>                                           
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="select" class="control-label mb-1">จำนวนชั้น</label>
                                                    <select name="layer" id="layer" class="form-control">
                                                    <option value="">โปรดเลือก</option>
                                                        <option value="1">1 ชั้น</option>
                                                        <option value="2">2 ชั้น</option>
                                                        <option value="3">3 ชั้น</option>
                                                        <option value="4">4 ชั้น</option>
                                                        <option value="5">5 ชั้น</option>
                                                        <option value="6">มากกว่า 5 ชั้น</option>      
                                                    </select>                                            
                                                </div>
                                            </div>
                                                                       
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label for="select" class="control-label mb-1">งบประมาณ</label>
                                                    <select name="cost" id="cost" class="form-control">
                                                    <option value="">โปรดเลือก</option>
                                                        <option value="1000000">1,000,000 - 2,000,000 บ.</option>
                                                        <option value="2000001">2,000,001 - 3,000,000 บ.</option>
                                                        <option value="3000001">3,000,001 - 4,000,000 บ.</option>
                                                        <option value="4000001">4,000,001 - 5,000,000 บ.</option>
                                                        <option value="5000001">5,000,001 - 6,000,000 บ.</option>
                                                        <option value="6000001">6,000,001 - 7,000,000 บ.</option>
                                                        <option value="7000001">มากกว่า 7,000,000 บ.</option>
                                                    </select>                                                  
                                                </div>
                                            </div>

                                            <div class="col-3">
                              <br>
                              <button id="savebutton" name="savebutton" type="submit" class="btn btn-lg btn-info btn-block">
                                  <i class="fa fa-lock fa-lg"></i>&nbsp;
                                  <span id="payment-button-amount">ตกลง</span>
                                  <span id="payment-button-sending" style="display:none;">Sending…</span>
                              </button>
                          </div> 
                          </div> 
                          </div> 

                                        </form>
                                    </div>
                                </div>
                            </div>
                            </center>
                            </div>


                            <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                    <h3 class="title-5 m-b-35">data table</h3>
                                    </div>
                                    
                                    <div class="table-data__tool-right">
                                        <button onclick="location.href = 'http://happyhomebuilder.co.th/admin/HDinsert.php';"class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>add item</button>
                                    
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr >
                                                <th></th>
                                                <th>รูปภาพ</th>
                                                <th></th>
                                                <th>รหัสแบบบ้าน</th>
                                                <th>จำนวนผู้เข้าชม</th>
                                                <th>ระบบก่อสร้าง</th>
                                                <th>รูปแบบ (สไตล์)</th>
                                                <th>จำนวนชั้น</th>
                                                <th>ที่ดินทั้งหมด</th>
                                                <th>พื้นที่ใช้สอย</th>
                                                <th>หน้ากว้าง</th>
                                                <th>งบประมาณ</th>
                                                <th>ห้องนอน</th>
                                                <th>ห้องน้ำ</th>
                                                <th>ห้องครัว</th>
                                                <th>ห้องรับประทานอาหาร</th>
                                                <th>ห้องรับแขก</th>
                                                <th>ห้องเก็บของ</th>
                                                <th>ห้องพระ</th>
                                                <th>ระเบียง</th>
                                                <th>ห้องแม่มบ้าน</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
include '../connect.php'; 

if (isset($_GET['pageno'])) {
  $pageno = $_GET['pageno'];
} else {
  $pageno = 1;
}
$no_of_records_per_page = 5;
$offset = ($pageno-1) * $no_of_records_per_page;


$col=array("style","land","size","cost","home_type","layer","product_id");
$colval=array("","","","","","","","F");


$url="";

for($i = 0 ; $i < count($col) ; $i++){
  if (isset($_GET[$col[$i]])) {
    if($_GET[$col[$i]]<> ''){
    $colval[$i] = (string) $_GET[$col[$i]];
    $url .="&".$col[$i]."=".$colval[$i]."";
    $colval[7] ="T";
    }
  }
}

if (isset($_GET['home_type'])&&isset($_GET['istype'])) {
  if($_GET["home_type"] =="" && $_GET["istype"] <> "0"){
    $colval[4] = $_GET["istype"];
  }
}


$strSQL = "";
$frist = 0;
for($i = 0 ; $i+1 < count($colval) ; $i++){
  if($colval[$i]<> ''){
    
    if($frist == 0){
      $frist = 1;
    }else{
      $strSQL .= " AND ";
    }
     if($i >= 1 and $i <=3){
      if(strlen($colval[$i])  == 6){
      $landmin = (int)substr($colval[$i], 0 ,3);
      $landmax = (int)substr($colval[$i], 3 ,6);
    }else if(strlen($colval[$i])>6){
      $landmin = (int)$colval[$i];
      if($landmin == 7000001 ){
        $landmax = $landmin * 10;
      }else{
        $landmax = $landmin + 999999;
      }
    }else{
      $landmin = (int)substr($colval[$i], 0 ,3);
      $landmax = $landmin *10;
    }
        $strSQL .= " (".$col[$i]." BETWEEN $landmin AND $landmax ) ";
    }else{
      $strSQL .= " homedetail.".$col[$i]." = '".$colval[$i]."' ";
  }
  }
}




if($colval[7] == "T"){
    $total_pages_sql = "SELECT COUNT(*) FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id  WHERE  {$strSQL} AND home_pic.show_order_pic = 1 " ;
    $sql = "SELECT * FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id ";
    $sql .= "LEFT JOIN totalview ON totalview.page=homedetail.product_id AND totalview.type =homedetail.home_type WHERE  {$strSQL} AND home_pic.show_order_pic = 1  ORDER BY `totalview`.`totalvisit` DESC LIMIT $offset, $no_of_records_per_page";
  }else{
    $total_pages_sql = "SELECT COUNT(*) FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id  WHERE home_pic.show_order_pic = 1  " ;
    $sql = "SELECT * FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id ";
    $sql .= "LEFT JOIN totalview ON totalview.page=homedetail.product_id AND totalview.type =homedetail.home_type WHERE home_pic.show_order_pic = 1 ORDER BY `totalview`.`totalvisit` DESC LIMIT $offset, $no_of_records_per_page";
  }
//   echo $sql;


$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

$res_data = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res_data)){
          echo " <tr class=\"tr-shadow\">";
          echo "<td>";
          echo "<div class=\"table-data-feature\">";
          echo "  <button onclick=\"location.href = 'http://happyhomebuilder.co.th/admin/Home.php?product_id=".$row["product_id"]."&home_type=".$row["home_type"]."';\" class=\"item\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\">";
          echo "      <i class=\"zmdi zmdi-edit\"></i>";
          echo "  </button>";
          echo " <button onclick=\"location.href = 'http://happyhomebuilder.co.th/admin/Home_delete.php?product_id=".$row["product_id"]."&home_type=".$row["home_type"]."';\"class=\"item\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\">";
          echo "   <i class=\"zmdi zmdi-delete\"></i>";
          echo " </button>";
          echo " </div>";
          echo "</td>";
          echo "<td colspan=\"2\">";
          echo "<img src='../imghome/".$row["product_id"]."/".$row["name_pic"]."' style= \"height: 80px; width : 100px;\">";
          echo " </td>";
          echo "<td>".$row["product_id"]."</td>";

          if($row["totalvisit"] == null){
            $visit = 0;
          }else{
            $visit  = $row["totalvisit"];
          }


          echo "<td class=\"desc\">".$visit."</td>";
          if($row["home_type"] == "CastinPlace"){
            echo "<td>ระบบหล่อในที่</td>";
          }else{
            echo "<td>ระบบสำเร็จรูป</td>";
          }
          echo "<td>".$row["style"]."</td>";
          echo "<td>".$row["layer"]."</td>";
          echo "<td>".$row["land"]."</td>";
          echo "<td>".$row["size"]."</td>";
          echo "<td>".$row["space"]."</td>";
          echo "<td>".$row["cost"]."</td>";
          echo "<td>".$row["bedroom"]."</td>";
          echo "<td>".$row["bathroom"]."</td>";
          echo "<td>".$row["kitchen"]."</td>";
          echo "<td>".$row["diningroom"]."</td>";
          echo "<td>".$row["livingroom"]."</td>";
          echo "<td>".$row["storeroom"]."</td>";
          echo "<td>".$row["tabernacle"]."</td>";
          echo "<td>".$row["balcony"]."</td>";
          echo "<td>".$row["housekeeper"]."</td>";
          echo "</tr><tr class=\"spacer\"></tr>";
    }
    mysqli_close($conn);
if($pageno <= $total_pages+1 ) {
  if($pageno <= 3 ) {
    $start = 1;
  }else if($total_pages-2 <$pageno){
    $start = $total_pages - 4;
  }else{
    $start = $pageno-2;
  }
}

?>
                                        </tbody>
                                    </table>
                                </div><br><br>

<center>
<nav aria-label="Page navigation example wow fadeInUp ">
  <ul class="pagination justify-content-center">
    <li class="page-item  <?php if($pageno <= 1){ echo 'disabled'; } ?>">
      <a class="page-link" href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=1".$url; } ?>" tabindex="-1">หน้าแรก</a>
    </li>
  
    <?php for($i=$start;$i<=$start+4;$i++){   ?> 
  <li class="page-item  <?php if( $i > $total_pages  ){ echo 'disabled'; }elseif($pageno ==$i){echo 'active';} ?>"><a class="page-link" href="<?php if( $i > $total_pages ){ echo '#'; } else { echo "?pageno=".$i.$url;}?>"><?php echo $i; ?></a></li>

 <?php } ?>

    <li class="page-item <?php if($total_pages == 0){ echo 'disabled'; }else if($total_pages == $pageno){ echo 'disabled';}?>">
      <a class="page-link  " href="<?php if($total_pages == 0 ){ echo '#'; }else { echo "?pageno=".$total_pages.$url;}?>">หน้าสุดท้าย</a>
    </li>
  </ul>
</nav>
</center>
</div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

</body>

</html>           