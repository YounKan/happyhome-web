<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Happy Home Builder</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="สร้างบ้าน ,รับสร้างบ้าน, ต่อเติม,แบบบ้าน,บ้านหล่อในที่ ,บ้านสำเร็จรูป, precast ,แถมรถ, แถมสระว่ายน้ำ, บ้านลด30% ,กู้ 100%, รับประกันโครงสร้าง 20ปี ,ลดครั้งใหญ่ ,สร้างทั่วไทย,ประสบการณ์ยาวนาน" name="keywords">
  <meta content="สร้างบ้าน รับสร้างบ้าน ต่อเติม แบบบ้าน   บ้านหล่อในที่ บ้านสำเร็จรูป precast แถมรถ แถมสระว่ายน้ำ บ้านลด 30% กู้ 100%  รับประกันโครงสร้าง 20ปี ลดครั้งใหญ่ สร้างทั่วไทย ประสบการณ์ยาวนาน" name="description">

  <!-- Favicons -->
  <link href="img/logoo.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/c.css" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- =======================================================
    Theme Name: Avilon
    Theme URL: https://bootstrapmade.com/avilon-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
  <style>
@font-face {
   font-family: "Nithan";
   src: url(lib/font-awesome/fonts/Nithan.ttf);
   font-weight: 700;
}

* {box-sizing: border-box}
body {font-family: "Nithan","Lato", sans-serif;}

.inline-block {
   display: inline-block;
   padding: 30px;
   width: 17vw;
}

.pictop{
  height: 350px;
  margin: -40px 0px 40px 0px;
}

 @media (max-width: 768px) {
  .inline-block {
   padding: 1px;
   width: 45vw;
}

.pictop{
  margin: -40px 0px 0px 0px;
}

      }
      .pagination {
    display: inline-block;
}

.pagination a {
    color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    border: 1px solid #ddd;
      border-radius: 5px;
}

.pagination a.active {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
      border-radius: 5px;
}

.pagination a:hover:not(.active) {background-color: #ddd;}
</style>
<script type="text/javascript">
$(document).ready(function()
{
    $('body').bind('contextmenu', function(e){
        return false;
    }); 
});


</script>
</head>
<body>

  <!--==========================
  <header id="header" style="background: linear-gradient(135deg, #ff6811, #ffae18);">
    Header 
  ============================-->
  <header id="header" style="background: linear-gradient(135deg, #ff6811, #ffae18);">
    <div class="container">
      <div id="logo" class="pull-left">
        <a href="index.php#home"><img src="img/logo1.png" alt="" title="" style="max-height: 50px; padding: 0px 0px 10px 0px;"></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
        <li><a href="index.php" style="padding-right: 15px; font-size: 24px;">หน้าแรก</a></li>
          <li><a href="about.php" style="padding-right: 15px; font-size: 24px;">เกี่ยวกับเรา</a></li>
          <li><a href="newpro.php" style="padding-right: 15px; font-size: 24px;">ข่าวและโปรโมชั่น</a></li>
          <li><a href="search.php" style="padding-right: 15px; font-size: 24px;">แบบบ้าน</a></li>
          <li><a href="spec.php" style="padding-right: 15px; font-size: 24px;">วัสดุที่ใช้</a></li>
          <li><a href="video.php" style="padding-right: 15px; font-size: 24px;">วีดีโอ</a></li>
          <li><a href="profile.php" style="padding-right: 15px; font-size: 24px;">บ้านผลงาน</a></li>
          <li><a href="contact.php" style="padding-right: 15px; font-size: 24px;">ติดต่อเรา</a></li>
          <li><a href="index_eng.php" style="padding-right: 15px; font-size: 24px;">EN</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header>
  
  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <!-- <script src="contactform/contactform.js"></script> -->

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>