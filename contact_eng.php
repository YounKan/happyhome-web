<!DOCTYPE html>
<html>
<?php include 'nav_eng.php'; 

include 'connect.php'; 


$conn = mysqli_connect($servername, $username, $password,$db);


if (!$conn) {

   die("Connection failed: " . mysqli_connect_error());

}
?>
  <body>
    <div id="map"><iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJVecqu5eX4jARqhIysVF8X5c&key=AIzaSyD3ioOZNLOYKL07MbPa2zxqipROxtP11Sc" allowfullscreen></iframe></div>
  <div id="address">
    <!-- Page Content -->
<div class="container" style=" background:rgba(255, 255, 255, 0.8); padding: 20px 50px 30px 50px;">

  <!-- Page Heading -->
  <h1 class="my-4">Contact Us
    <small> : Company-Map</small>
  </h1><hr>

  <!-- Project One -->
  <div class="row">
    <div class="col-md-7">
        <img class="img-fluid rounded mb-3 mb-md-0" src="img/kanjanapisek_1.jpg" alt="">
    </div>
    <div class="col-md-5">
        <h3>สำนักงานใหญ่กาญจนาภิเษก</h3>
        <p>เลขที่ 626/212  ถนนกาญจนาภิเษก แขวงบางไผ่ </p>
        <p>เขตบางแค กรุงเทพมหานคร 10160</p>
    </div>
  </div>
  <!-- /.row -->

  <hr>

  <!-- Project Two -->
  <div class="row">
    <div class="col-md-7">
        <img class="img-fluid rounded mb-3 mb-md-0" src="img/asoke.jpg" alt="">
    </div>
    <div class="col-md-5">
        <h3>สำนักงานสาขาอโศก</h3>
        <p>โครงการลุมพินีพลาซ่า</p>
        <p>เลขที่ 189/1 อาคารแกรนด์พาร์ควิว อโศก</p>
        <p>ถ.สุขึมวิท 2 แขวงคลองเตยเหนือ เขตวัฒนา กรุงเทพมหานคร</p>
    </div>
  </div>
  <!-- /.row -->

  <hr>

  <!-- Project Three -->
  <div class="row">
    <div class="col-md-7">
        <img class="img-fluid rounded mb-3 mb-md-0" src="img/na_nakorn.jpg" alt="">
    </div>
    <div class="col-md-5">
      <h3>สำนักงานสาขาแจ้งวัฒนะ</h3>
      <p>อาคาร ณ นคร เลขที่ 99/349 หมู่ 2 ถ.แจ้งวัฒนะ</p>
      <p>แขวงทุ่งสองห้อง เขตหลักสี่ กรุงเทพมหานคร</p>
    </div>
  </div>
  <!-- /.row -->

  <hr>

  <!-- Project Four -->
  <div class="row">

    <div class="col-md-7">
        <img class="img-fluid rounded mb-3 mb-md-0" src="img/21tower.jpg" alt="">
    </div>
    <div class="col-md-5">
      <h3>สำนักงานสาขาศรีนครินทร์</h3>
      <p>เลขที่ 805 ถนนศรีนครินทร์ แขวงสวนหลวง</p>
      <p>เขตสวนหลวง กรุงเทพมหานคร</p>
      <br/>	
      </div>
  </div>
  <!-- /.row --><br> 
</div></div>
<!-- /.container -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container"><br>
        <div class="row wow fadeInUp">

          <div class="col-lg-5 col-md-4">
            <div class="contact-about">             
            <h3>Contact Us</h3><hr>
              <h4>Happy Home Builder</h4>
              <h4>Tel : 02-449-5354</h4>
            </div>
          </div>
          <div class="col-lg-1 col-md-4">
          </div>

          <?php

if(isset($_POST['sub'])){
  $result = 0 ;

  if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['subject']) || empty($_POST['message']) || empty($_POST['tel'])){

    $error = "Please fill up the blank for your convenience.";
    $result = 0 ;
   
  }else{

      $sql = "INSERT INTO `contact`(`name`, `email`, `subject`, `message`, `tel`) VALUES ('".$_POST['name']."','".$_POST['email']."','".$_POST['subject']."','".$_POST['message']."','".$_POST['tel']."')";

      $db = mysqli_query( $conn,$sql);
      $error ="Your message has been sent. Thank you!";
      $result = 1 ;
  }
}

?>
          <div class="col-lg-5 col-md-8">
            <div class="form">
            <?php if (isset($_POST['sub'])) {
				if($result == 1  ) {
				echo "<div class=\"alert alert-success\">$error </div>";
				}else{
				echo "<div class=\"alert alert-danger\">$error </div>";
				}
			}			
			?>
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="" method="post" role="form" >
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="กรุณาใส่อย่างน้อย 4 ตัวอักษร" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="กรุณาใส่อีเมลล์" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="tel" id="tel" placeholder="Tel" data-rule="minlen:4" data-msg="กรุณาใส่อย่างน้อย 8 ตัวอักษร" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="กรุณาใส่อย่างน้อย 8 ตัวอักษร" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="กรุณาใส่ข้อความ" placeholder="ข้อความ"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button name="sub"  type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  <!--==========================
    Footer
  ============================-->
  <?php include 'footer_eng.php';?>

</body>
</html>
