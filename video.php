<?php include 'nav.php'; ?>
<main id="main">
<section id="vdo" >
      <div class="container">
<br><br>
        <div class="section-header"><br><br><br>
          <h3 class="section-title">วีดีโอ</h3>
          <span class="section-divider"></span>
        </div>
       
        <div class="row">
          <div class="col-lg-8 box video-item">
          <?php

if (isset($_GET['videono'])) {
    $videono = $_GET['videono'];
} else {
    $videono = 1;
}

include 'connect.php'; 

$strSQL = "SELECT * FROM content WHERE section = 'video' AND show_id  = $videono ";
$objQuery =mysqli_query($conn,$strSQL);
if(!$objQuery){	
echo "fail";
  }else{
      while($row = mysqli_fetch_assoc($objQuery)){
      $videoName = $row["content_pic"];
      $videoTitle = $row["title_TH"];
      $videoContent = $row["content_TH"];
  }
  $videoName = substr($videoName,32);
}?>

          <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $videoName; ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInLeft">
              <h4 class="title" style="margin-left: 0px;"><?php echo $videoTitle; ?></h4>
              <p class="description" style="margin-left: 0px;"><?php echo $videoContent; ?></p>
            </div>
          </div>


         <div class="col-lg-6">
            <div class="box wow fadeInRight">
            <a href="video.php?videono=1"> <div class="icon"><img src="https://img.youtube.com/vi/mjeDbBcUuN0/0.jpg"  style= "width: 180px; height: 100px;" alt=""></div> </a>
              <h4 class="videotxt">บันทึกรายการเปิดบ้าน</h4>
              <p class="videotxt">บริษัทแฮปปี้โอมบิวเดอร์บันทึกรายการ "อยู่สบาย" ในช่วง "เปิดบ้าน" ดำเนินรายการ โดย อาจารย์เอกพงษ์ ตรีตรง</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight">
            <a href="video.php?videono=2"> <div class="icon"><img src="https://img.youtube.com/vi/TOt_-5v8me8/0.jpg"  style= "height: 100px;" alt=""></div> </a>
              <h4 class="videotxt">บ้านคุณพิศมร พรหมด้วง</h4>
              <p class="videotxt">ผลงานการก่อสร้างบ้านพักอาศัย 3 ชั้น ใจกลางเมือง สวยสง่างามทรงคุณค่า</p>
            </div>
          </div>

          <div class="col-lg-3"></div>
          <div class="col-lg-6">
            <div class="box wow fadeInRight">
            <a href="video.php?videono=3"> <div class="icon"><img src="https://img.youtube.com/vi/2f_bmXi4Eig/0.jpg"  style= "height: 100px;" alt=""></div> </a>
              <h4 class="videotxt">บ้านคุณอนุชัย โต๊ะสมัน</h4>
              <p class="videotxt">ผลงานการก่อสร้าง บ้านคุณอนุชัย โต๊ะสมัน</p><br>
            </div>
          </div>
          <div class="col-lg-3"></div>

        </div>
      </div>
    </section><!-- #more-features -->
</main>
<?php include 'footer.php';?>
</body>
</html>