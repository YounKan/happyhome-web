<?php include 'nav_eng.php'; 

if (isset($_GET['homeno']) && isset($_GET['hometype']) ) {
  $homeono = $_GET['homeno'];
  $hometype = $_GET['hometype'];

  include 'connect.php'; 

  $user_ip=$_SERVER['REMOTE_ADDR'];

$datetime = date("Y-m-d H:i:s");

$check_ip = mysqli_query( $conn,"select * from pageview where page='{$homeono}' and type ='{$hometype}' and userip='$user_ip'");
if(mysqli_num_rows($check_ip)>=1)
{
  while($row = mysqli_fetch_assoc($check_ip)){
    $date = $row["enter_time"];
  }

  if (strtotime($date) < strtotime("-1 day")){
    $updateview = mysqli_query($conn,"update pageview set enter_time = '$datetime' where page='{$homeono}'and type ='{$hometype}' ");
    $updateview = mysqli_query($conn,"update totalview set totalvisit = totalvisit+1 where page='{$homeono}'and type ='{$hometype}' ");
  }
}
else
{

$insertview = mysqli_query($conn,"insert into pageview values('','{$homeono}','{$hometype}','$user_ip','$datetime')");

$check_ip = mysqli_query( $conn,"select * from totalview where page='{$homeono}' and type ='{$hometype}' ");
if(mysqli_num_rows($check_ip)>=1)
{
  $updateview = mysqli_query($conn,"update totalview set totalvisit = totalvisit+1 where page='{$homeono}' and type ='{$hometype}'  ");
}else{
  $insertview = mysqli_query($conn,"insert into totalview values('','{$homeono}','{$hometype}','1')");
}
}



  $colcontent=array("product_id","style","layer","land","size","space","cost","bedroom","bathroom","kitchen","diningroom","livingroom","storeroom","tabernacle","balcony","housekeeper");
  $arr_valcontent = array();
  $arr_pic = array();

  $strSQL = "SELECT * FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id WHERE homedetail.product_id = '{$homeono}'AND homedetail.home_type = '{$hometype}' order by show_order_pic ";
  $countSQL = "SELECT COUNT(*) FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id  WHERE homedetail.product_id = '{$homeono}' AND homedetail.home_type = '{$hometype}'";

  

  $result = mysqli_query($conn,$countSQL);
  $count = mysqli_fetch_array($result)[0];

  $i = 0;

  $objQuery =mysqli_query($conn,$strSQL);
  while($row = mysqli_fetch_assoc($objQuery)){
        array_push($arr_pic,$row["name_pic"]);
        $i++;

      if($i == $count){

        for($j = 0 ; $j < count($colcontent) ; $j++){ 
          array_push($arr_valcontent,$row[$colcontent[$j]]);
        }
      }
  }
}

?>
<section id="home">
      <div class="container-fluid">
      <br><br> <br><br> <br><br>
        <div class="section-header">
          <h3 class="section-title"><?php echo  $arr_valcontent[0];?></h3> 
          <span class="section-divider" style="width : 200px;"></span>
          <br>
        </div>
        <div class="row no-gutters">
          <?php
          for($i = 0 ; $i < count($arr_pic) ; $i++){ 
           echo "<div class=\"col-lg-6 col-md-6\">";
           echo "<div class=\"gallery-item wow fadeInUp\">";
           echo "<a href=\"imghome/".$arr_valcontent[0]."/".$arr_pic[$i]."\" class=\"gallery-popup\">";
           echo "<img src=\"imghome/".$arr_valcontent[0]."/".$arr_pic[$i]."\" alt=\"\">";
           echo " </a>";
           echo "</div>";
           echo "</div>";
          }
          ?>
          </div>
        </div>
  </section>


    <!--==========================
      More Features Section
    ============================-->
    <section id="more-features" class="section-bg">
      <div class="container" style="text-align: center;">

        <div class="section-header">
          <h3 class="section-title">Details</h3>
          <span class="section-divider"></span>
        </div><hr>

        <div class="row">

          <div class="col-lg-4">
            <div class="box wow fadeInLeft">
              <div class="icon"><img src="img/icon/style.png" alt="Style house" height="90" width="90"></div>
              <h4 class="title" >Style</a></h4>
              <p class="description"><?php echo  $arr_valcontent[1];?></p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInRight">
              <div class="icon"><img src="img/icon/level.png" alt="Level house" ></div>
              <h4 class="title">Storey</a></h4>
              <p class="description"><?php echo  $arr_valcontent[2];?></p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInLeft">
              <div class="icon"><img src="img/icon/front.png" alt="Size house" ></div>
              <h4 class="title">Land</a></h4>
              <p class="description"><?php echo  $arr_valcontent[3];?> Sq wah</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight">
              <div class="icon"><img src="img/icon/size.png" alt="Front house" height="90" width="90"></div>
              <h4 class="title">Width-Depth</a></h4>
              <p class="description"><?php echo  $arr_valcontent[5];?> m</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInLeft">
              <div class="icon"><img src="img/icon/price.png" alt="Price" height="90" width="90"></div>
              <h4 class="title">Price</a></h4>
              <p class="description"><?php echo  $arr_valcontent[6];?> Baht</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="box wow fadeInLeft">
              <div class="icon"><img src="img/icon/bedroom.png" alt="Bedroom" height="90" width="90"></div>
              <h4 class="title">Bedroom</a></h4>
              <p class="description"> <?php echo  $arr_valcontent[7];?> rooms </p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInRight">
              <div class="icon"><img src="img/icon/bathroom.png" alt="Bathroom" height="90" width="90"></div>
              <h4 class="title">Bathroom</a></h4>
              <p class="description"><?php echo  $arr_valcontent[8];?> rooms</p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInLeft">
              <div class="icon"><img src="img/icon/kitchen.png" alt="Kitchen" height="90" width="90"></div>
              <h4 class="title">Kitchen</a></h4>
              <p class="description"><?php echo  $arr_valcontent[9];?> rooms</p>
            </div>
          </div>
        </div>
      </div>
    </section><!-- #more-features -->
</main>
<?php include 'footer_eng.php';?>
</body>
</html>