<?php
include 'connect.php'; 

$datetime = date("Y-m-d H:i:s");
$datetimenew = date("Y-m-d 00:00:00");

$check_day = mysqli_query( $conn,"SELECT * FROM day_view WHERE day = '$datetimenew' ");
if(mysqli_num_rows($check_day)>=1)
{
  while($row = mysqli_fetch_assoc($check_day)){
	  $todaycount =  $row["dayvisit"]; 
  }
}

$dayofweek = date('w', strtotime($datetime)) +1;

$check_week = mysqli_query( $conn,"SELECT * FROM day_view ORDER BY id DESC LIMIT $dayofweek");
if(mysqli_num_rows($check_week)>=1)
{
	$total_week = 0;
  while($row = mysqli_fetch_assoc($check_week)){
    $total_week += $row["dayvisit"];
  }
}

$month = date("m");

$check_month = mysqli_query( $conn,"SELECT * FROM day_view ");
if(mysqli_num_rows($check_month)>=1)
{
	$total_month = 0;
  while($row = mysqli_fetch_assoc($check_month)){
	$newDate = date("m", strtotime($row["day"]));
	  if($month == $newDate){
		$total_month += $row["dayvisit"];
	  }
  }
}

$check_total = mysqli_query( $conn,"SELECT * FROM totalview where page='happyhome' ");
if(mysqli_num_rows($check_total)>=1)
{
  while($row = mysqli_fetch_assoc($check_total)){
	  if($month == $newDate){
		$total = $row["totalvisit"];
	  }
  }
}

$static = "สถิตคนเข้าชมเว็บไซต์ ทั้งหมด : ".$total." คน,  เดือน : ".$total_month." คน,  สัปดาห์ : ".$total_week." คน,  วันนี้ : ".$todaycount." คน";

?>

<footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <img src="img/footer.png" class="imgfooter">
          <h3>รับสร้างบ้าน สร้างบ้าน ต่อเติม บ้านหล่อในที่ บ้านสำเร็จรูป แฮปปี้โฮมบิวเดอร์ วัสดุก่อสร้าง</h3>
          <h4>www.happyhomebuilder.com | สำนักงานใหญ่ตลิ่งชัน เลขที่ 626/212 ถนนกาญจนาภิเษก แขวงบางไผ่ เขตบางแค กทม. 10160 | โทร : 02-449-5354</h4>
          <h4 style="color: red;">บริษัทขอสงวนสิทธิเปลี่ยนแปลงราคาโดยมิต้องแจ้งให้ทราบล่วงหน้า</h4>
          <h4><?php echo $static;?></h4><br></div>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="search.php" class="homedesign-to"><center><i class="fa">แบบบ้าน</i></center></a>
  <a href="newpro.php" class="promo-to"><center><i class="fa">โปรโมชั่น</i></center></a>
  <a href="index.php#contact" class="contact-to"><center><i class="fa">ติดต่อสอบถาม</i></center></a>