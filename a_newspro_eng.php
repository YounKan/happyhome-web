<?php include 'nav_eng.php'; ?>
<?php

if (isset($_GET['pronewsno'])) {
    $pronewsno = $_GET['pronewsno'];
} else {
    $pronewsno = 1;
}

include 'connect.php'; 

$arr_pic = array();

$strSQL = "SELECT * FROM `pro_news` LEFT JOIN pro_news_detail ON pro_news.pro_news_id=pro_news_detail.pro_news_id  WHERE pro_news_detail.pro_news_id  = $pronewsno AND pro_news_detail.type = 'img' order by show_order_pic ";

$objQuery =mysqli_query($conn,$strSQL);
while($row = mysqli_fetch_assoc($objQuery)){
    array_push($arr_pic,$row["name"]);
    $maintype = $row["maintype"];
    $title = $row["title"];
    $subtitle = $row["subtitle"];
  }

  if($maintype == "news"){
    $maintype = "News";
  }else{
    $maintype ="Promotions";
  }

?>
<main id="main">
<section id="anewspro" >
      <div class="container">
<br><br>
        <div class="section-header"><br><br><br>
          <h2 class="section-title"><?php echo $maintype;?> : <?php echo $title;?></h2>
          <span class="section-divider"></span>
          <!-- <p class="section-description boxsubtitle"><?php echo $subtitle;?></p> -->
        </div>
       
        <div class="row">
          <div class="col-lg-12  imgpro">
          <?php

for($i = 0 ; $i < count($arr_pic) ; $i++){ 
echo " <img src=\"img/pro_news/$pronewsno/$arr_pic[$i]\" alt=\"\">";
}
?>

          </div>

        </div>
      </div>
    </section><!-- #more-features -->
    <?php


$arr_home = array();

$sqlhome = "SELECT * FROM `pro_news` LEFT JOIN pro_news_detail ON pro_news.pro_news_id=pro_news_detail.pro_news_id  WHERE pro_news_detail.pro_news_id  = $pronewsno AND pro_news_detail.type = 'homedesign'";

$objQueryhome =mysqli_query($conn,$sqlhome);
while($row = mysqli_fetch_assoc($objQueryhome)){
    array_push($arr_home,$row["name"]);
  }

?>
<?php 

if(count($arr_home) <> 0){
echo "<section id=\"team\" >";
echo "<div class=\"container\">";
echo "<div class=\"section-header\">";
echo "<h3 class=\"section-title\">House plans</h3>";
echo "<span class=\"section-divider\"></span>";
echo "</div>";
echo "<div class=\"row wow fadeInUp\">";
        

for($i = 0 ; $i < count($arr_home) ; $i++){ 

  $sql = "SELECT * FROM homedetail LEFT JOIN home_pic ON home_pic.product_id=homedetail.product_id ";
  $sql .= " LEFT JOIN totalview ON totalview.page=homedetail.product_id WHERE home_pic.show_order_pic = 1 AND homedetail.product_id = '$arr_home[$i]' AND homedetail.home_type = 'CastinPlace' ";
  $objQuery =mysqli_query($conn,$sql);
  while($row = mysqli_fetch_assoc($objQuery)){
    echo "<div class=\"col-lg-4\">";
    echo "<div class=\"member\">";
    echo "<div class=\"pic\"><img src=\"imghome/".$row["product_id"]."/".$row["name_pic"]."\"></div>";
    echo "<div style=\"background: #fff; padding: 10px 10px 10px 10px; \">";
    echo "<a href=\"home_eng.php?homeno=".$row["product_id"]."&hometype=".$row["home_type"]."\"><h1><img src=\"img/icon/home.png\" alt=\"House\" height=\"40\" width=\"40\"> ".$row["product_id"]."</a></h1><hr>";
    echo "<div class=\"box col-lg-5\"><p>Style : ".$row["style"]."</p>";
    
    if($row["home_type"] == "CastinPlace"){
      echo "</div><div class=\"box col-lg-7\"><p>Construction : Cast In Place</p>";
    }else{
      echo "</div><div class=\"box col-lg-7\"><p>Construction : Precast Concrete System</p>";
    }
  
    echo "</div><div class=\"box col-lg-7\"><p>Useful Space : ".$row["size"]." Sq m</p>";
    echo "</div><div class=\"box col-lg-5\"><p>Storey : ".$row["layer"]."</p>";
    echo "</div><div class=\"box col-lg-12\"><p>Width-Depth : ".$row["space"]." m</p>";
    echo "</div><div><center><p>Land : ".$row["land"]." Sq wah</p></center>";
    echo "</div><div><center><p style=\" font-size: 30px; color: orange;\">Price : ".number_format($row["cost"]).".-</p></center>";
    if($row["totalvisit"] == null){
      $visit = 0;
    }else{
      $visit  = $row["totalvisit"];
    }
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
    }
}
    
echo "</div>";
echo "</div>";
echo "</section>";
}
    ?>  
 <?php include 'footer_eng.php';?>
</main>
</body>
</html>